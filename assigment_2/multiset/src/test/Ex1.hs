module Ex1 where

data ListBag a = LB [(a, Int)]
  deriving (Show, Eq)

wf :: Eq a => ListBag a -> Bool

wf container = True
