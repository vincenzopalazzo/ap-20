/**
 * Advanced Programming project 2020/2021
 * <p>
 * Copyright (C) 2020-2021 Vincenzo Palazzo vincenzopalazzodev@gmail.com
 * and all professors of the Advanced Programming class
 * http://pages.di.unipi.it/corradini/Didattica/AP-20/
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

package it.unipi.vincenzopalazzo;

import it.unipi.vincenzopalazzo.exceptions.BusException;
import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import mdlaf.MaterialLookAndFeel;
import mdlaf.themes.MaterialOceanicTheme;
import mdlaf.themes.MaterialTheme;
import mdlaf.utils.MaterialBorders;
import mdlaf.utils.MaterialColors;
import mdlaf.utils.MaterialImageFactory;
import mdlaf.utils.icons.MaterialIconFont;
import org.material.component.swingsnackbar.SnackBar;
import org.material.component.swingsnackbar.action.AbstractSnackBarAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is the Component UI of the application and it is
 * coupled only with the model, so this class have no dependencies with the
 * Covid Controller.
 *
 * @author Vincenzo Palazzo <v.palazzo1@studenti.unipi.it>
 */
public class BusBoard extends JFrame {

    private static final Logger LOGGER = LoggerFactory.getLogger(BusBoard.class);
    
    private Bus busModel;

    /**
     * A UI propriety to have access to the theme color in the application.
     */
    private MaterialTheme theme;

    /**
     * Some UI component to make a very simple MenuBar
     */
    private JMenuBar menuBar;
    private JMenu fileMenu;
    private JMenuItem exitItem;

    /**
     * The component used to display the information in a very simple way
     */
    /**
     * The capacityLabel label display all the information inside Bus model
     */
    private JLabel capacityLabel;
    /**
     * The doorOpenLabelImg contains the information about the door status
     */
    private JLabel doorOpenLabelImg;
    /**
     * lockIcon and unlockIcon are simple icon to
     * help the user to understand with a icon if the dor is locked or unlocked
     */
    private Icon lockIcon;
    private Icon unlockIcon;
    /**
     * RangeSpinner is the UI component to put inside a num of passenger between 1 and 5
     */
    private JSpinner rangeSpinner;
    /**
     * The enter button is used to submit a number of passenger selected with the jSpinner
     */
    private JButton enterButton;
    /**
     * The PanelContainer used to contains the main UI, like the root dir with ReactJS
     */
    private JPanel panelContainer;
    /**
     * Material Component to display the error like the Vetoable exception to the user.
     */
    private SnackBar snackBar;

    /**
     * The Propriety change listener user to listen the change of the Bus model.
     */
    private PropertyChangeListener changeValues;


    /**
     * The default constructor is the unique constructor of the app
     * that is used to initialize the theme and the propriety change listener
     * with the local implementation
     */
    public BusBoard() {
        super("Covid Busses");
        try {
            theme = new MaterialOceanicTheme();
            UIManager.setLookAndFeel(new MaterialLookAndFeel(theme));
        } catch (UnsupportedLookAndFeelException ex) {
            LOGGER.error(ex.getLocalizedMessage());
        }
        this.changeValues = new ObserveChangeValues();
    }

    /**
     * In the App is build a Swing life cycle to initialize the app in a correct way.
     *
     * The method initView is the first method used to inflate the content of the app.
     * An analogy can be the method onCreate on Android Framework
     */
    public void initView() {
        this.busModel = new Bus();
        this.menuBar = new JMenuBar();
        this.fileMenu = new JMenu("File");
        this.exitItem = new JMenuItem("Exit");
        Icon exitIcon = MaterialImageFactory.getInstance().getImage(
                MaterialIconFont.EXIT_TO_APP, theme.getTextColor());
        this.exitItem.setIcon(exitIcon);
        
        this.fileMenu.add(exitItem);
        this.menuBar.add(this.fileMenu);
        this.setJMenuBar(this.menuBar);

        this.lockIcon = MaterialImageFactory.getInstance().getImage(MaterialIconFont.LOCK, theme.getTextColor());
        this.unlockIcon = MaterialImageFactory.getInstance().getImage(MaterialIconFont.LOCK_OPEN, theme.getTextColor());

        var numPassenger = busModel.getNumPassengers();
        this.capacityLabel = new JLabel(String.valueOf(numPassenger));
        var isOpen = busModel.isDoorOpen() ? "Open" : "Close";
        this.doorOpenLabelImg = new JLabel(isOpen);
        this.doorOpenLabelImg.setIcon(busModel.isDoorOpen() ? this.unlockIcon : this.lockIcon);

        this.enterButton = new JButton("Enter");
        this.rangeSpinner = new JSpinner(new SpinnerNumberModel(1, 1, 5, 1));
        this.panelContainer = new JPanel(new BorderLayout());
        
        this.initLayout();
        this.initActions();
        
        this.setContentPane(panelContainer);
        this.setDefaultCloseOperation(this.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        //this.pack();
        this.setSize(500, 300);
        this.setVisible(true);
    }

    /**
     * initLayout is the second method of the life cycle of the app, this method
     * is called inside the initView and the work of it is to set up the layout of the App
     * with the Swing rules.
     */
    private void initLayout() {
        JPanel headerPanel = new JPanel(new BorderLayout());
        headerPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        headerPanel.add(this.capacityLabel, BorderLayout.EAST);
        headerPanel.add(this.doorOpenLabelImg, BorderLayout.WEST);
        panelContainer.add(headerPanel, BorderLayout.NORTH);
        
        JPanel bodyPanel = new JPanel(new BorderLayout());
        bodyPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createEmptyBorder(10, 10, 10, 10), 
                MaterialBorders.DEFAULT_SHADOW_BORDER
            )
        );
        bodyPanel.add(this.rangeSpinner);
        panelContainer.add(bodyPanel, BorderLayout.CENTER);
        panelContainer.add(this.enterButton, BorderLayout.SOUTH);
    }

    /**
     * The method initActions is the last method of the lifecycle of the app used to
     * setup the action inside the component, is called by initView after the initLayout method.
     * At this point all the object are not null and we can avoid all the null check.
     */
    private void initActions() {
        this.enterButton.addActionListener((event) -> {
            try {
                int value = (int) rangeSpinner.getValue();
                busModel.setNumPassengers(value);
            } catch (BusException ex) {
                Icon close = MaterialImageFactory.getInstance().getImage(MaterialIconFont.CLOSE, MaterialColors.COSMO_BLACK);
                if (snackBar == null)
                    snackBar = SnackBar.make(this, "The num of passengers is wrong", close)
                            .setDuration(SnackBar.LENGTH_LONG)
                            .setPosition(SnackBar.TOP)
                            .setSnackBarBackground(MaterialColors.COSMO_MEDIUM_GRAY)
                            .setSnackBarForeground(MaterialColors.COSMO_BLACK)
                            .setAction(new AbstractSnackBarAction() {
                                @Override
                                public void mousePressed(MouseEvent e) {
                                    snackBar.dismiss();
                                }
                            }) ;
                snackBar.run();
            }
        });
        
        this.exitItem.addActionListener((event) -> {
            System.exit(0);
        });
        
        this.busModel.addProprietyListener(changeValues);
    }

    /**
     * PropertyChangeListener implementation, it used to listen the changes inside the application
     * and with this change the UI is updated.
     *
     * @author Vincenzo Palazzo <v.palazzo1@studenti.unipi.it>
     */
    private class ObserveChangeValues implements PropertyChangeListener {
        

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            switch (evt.getPropertyName()) {
                case Bus.DOOR_OPEN -> {
                    boolean value = (boolean) evt.getNewValue();
                    LOGGER.debug("Door open change listener with value " + value);
                    doorOpenLabelImg.setText(value ? "Open" : "Close");
                    doorOpenLabelImg.setIcon(value ? unlockIcon : lockIcon);
                }
                case Bus.NUM_PASSENGERS -> {
                    LOGGER.debug("Num passengers listener");
                    int value = (int) evt.getNewValue();
                    capacityLabel.setText(String.valueOf(value));
                }
            }
        }
        
    }
}
