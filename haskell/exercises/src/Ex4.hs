module Ex4 where

totalLengthR [] = 0
totalLengthR (x:xs) =
  (if (head x) == 'A' then Prelude.length x else 0) + totalLengthR xs

totalLengthC list =
  foldl (+) 0 (map(\x -> (if (head x) == 'A' then Prelude.length x else 0)) list)
