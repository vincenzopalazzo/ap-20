module Ex3 where

import Ex1

replR [] n = []
replR (x:xs) n = (myReplicateR n x) ++ (replR xs n)

replC :: (Show a, Integral n) => [a] -> n -> [a]
replC list n =
  foldl (++) [] (map (\x -> (myReplicateC n x)) list)
