/**
 * Advanced Programming project 2020/2021
 * <p>
 * Copyright (C) 2020-2021 Vincenzo Palazzo vincenzopalazzodev@gmail.com
 * and all professors of the Advanced Programming class
 * http://pages.di.unipi.it/corradini/Didattica/AP-20/
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package it.unipi.vincenzopalazzo.buscovid.controller;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import javax.lang.model.SourceVersion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Vincenzo Palazzo <v.palazzo1@studenti.unipi.it>
 */
public class CovidController implements VetoableChangeListener {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(CovidController.class);

    /**
     * The propriety throws by the vetoable change listener
     */
    public static final String NUM_PASSENGER = "NUM_PASSENGER";

    /**
     * The reduce capacity with a default value set to 25
     */
    private int reducedCapacity;

    /**
     * The default construct of the class it the unique too,
     * when this construct is called a the default value for the reduce capacity is set.
     */
    public CovidController() {
        this.reducedCapacity = 25;
    }

    /**
     * The implementation of the abstract method vetoChangeshould forbid the 
     * change of the property numPassengers the new value is larger than 
     * the reducedCapacity.
     * 
     * @param evt The event from the Java Swing framework Life cicle
     * @throws PropertyVetoException An exception that that to the receiver that the new value is out of bound
     */
    @Override
    public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
        if (evt.getPropertyName().equals(NUM_PASSENGER)) {
            int value = (int) evt.getNewValue();
            LOGGER.debug("Value in the vetoable listener is " + value);
            LOGGER.debug("The reduce capatiry is: " + this.reducedCapacity);
            if (value > this.reducedCapacity) {
                String message = String.format("Capacity %d was rejected, the maximum capacity is %d", value, this.reducedCapacity);
                throw new PropertyVetoException(message, evt);
            }
        }
    }
}
