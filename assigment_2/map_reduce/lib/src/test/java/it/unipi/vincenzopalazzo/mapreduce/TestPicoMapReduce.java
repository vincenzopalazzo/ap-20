/**
 * Advanced Programming project 2020/2021
 * <p>
 * Copyright (C) 2020-2021 Vincenzo Palazzo vincenzopalazzodev@gmail.com
 * and all professors of the Advanced Programming class
 * http://pages.di.unipi.it/corradini/Didattica/AP-20/
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package it.unipi.vincenzopalazzo.mapreduce;

import it.unipi.vincenzopalazzo.mapreduce.exception.MapReduceException;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/*
 * @author Vincenzo Palazzo <v.palazzo1@studenti.unipi.it>
 */
public class TestPicoMapReduce {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestPicoMapReduce.class);

    private PicoMapReduce mapReduce;

    public TestPicoMapReduce() {
        this.mapReduce = new PicoMapReduce();
    }

    @Before
    public void cleanMapReduce() {
        mapReduce.setAbsoluteOutputPath(null);
        mapReduce.setAbsoluteInputPath(null);
    }

    @Test(expected = MapReduceException.class)
    public void inputPathNull() {
        mapReduce.start();
    }

    /**
     * Check if the file produced is not null and make sure that all the process works
     */
    @Test
    public void readInputAChristmasCarol() {
        String path = this.getClass().getClassLoader().getResource("A_Christmas_Carol.txt").getPath();
        LOGGER.debug("Path: " + path);
        mapReduce.setAbsoluteInputPath(path);
        mapReduce.start();
        TestCase.assertNotNull(this.getClass().getClassLoader().getResource("A_Christmas_Carol.txt.csv"));
    }

    @Test
    public void readInputAChristmasCarolCountResult() throws IOException {
        String path = this.getClass().getClassLoader().getResource("A_Christmas_Carol.txt").getPath();
        LOGGER.debug("Path: " + path);
        mapReduce.setAbsoluteInputPath(path);
        mapReduce.start();
        TestCase.assertNotNull(this.getClass().getClassLoader().getResource("A_Christmas_Carol.txt.csv"));
        path = this.getClass().getClassLoader().getResource("A_Christmas_Carol.txt.csv").getPath();
        Path realPath = Path.of(path);
        var allLines = Files.lines(realPath);
        allLines.forEach(LOGGER::debug);
        int lines = (int) Files.lines(realPath).map(x -> x.split(",")).count();
        LOGGER.debug("Size after splitting with ',' " + lines);
        TestCase.assertTrue(lines > 100);
    }

    /**
     * Check if the file produced is not null and make sure that all the process works
     */
    @Test
    public void readInputFrankenstein() {
        String path = this.getClass().getClassLoader().getResource("Frankenstein.txt").getPath();
        LOGGER.debug("Path: " + path);
        mapReduce.setAbsoluteInputPath(path);
        mapReduce.start();
        TestCase.assertNotNull(this.getClass().getClassLoader().getResource("Frankenstein.txt.csv"));
    }

    @Test
    public void readInputFrankensteinCountResult() throws IOException {
        String path = this.getClass().getClassLoader().getResource("Frankenstein.txt").getPath();
        LOGGER.debug("Path: " + path);
        mapReduce.setAbsoluteInputPath(path);
        mapReduce.start();
        TestCase.assertNotNull(this.getClass().getClassLoader().getResource("Frankenstein.txt.csv"));
        path = this.getClass().getClassLoader().getResource("Frankenstein.txt.csv").getPath();
        Path realPath = Path.of(path);
        var allLines = Files.lines(realPath);
        allLines.forEach(LOGGER::debug);
        int lines = (int) Files.lines(realPath).map(x -> x.split(",")).count();
        LOGGER.debug("Size after splitting with ',' " + lines);
        TestCase.assertTrue(lines > 100);
    }

    /**
     * Check if the file produced is not null and make sure that all the process works
     */
    @Test
    public void readInputMetamorphosis() {
        String path = this.getClass().getClassLoader().getResource("Metamorphosis.txt").getPath();
        LOGGER.debug("Path: " + path);
        mapReduce.setAbsoluteInputPath(path);
        mapReduce.start();
        TestCase.assertNotNull(this.getClass().getClassLoader().getResource("Metamorphosis.txt.csv"));
    }

    @Test
    public void readInputMetamorphosisCountResult() throws IOException {
        String path = this.getClass().getClassLoader().getResource("Metamorphosis.txt").getPath();
        LOGGER.debug("Path: " + path);
        mapReduce.setAbsoluteInputPath(path);
        mapReduce.start();
        TestCase.assertNotNull(this.getClass().getClassLoader().getResource("Metamorphosis.txt.csv"));
        path = this.getClass().getClassLoader().getResource("Metamorphosis.txt.csv").getPath();
        Path realPath = Path.of(path);
        var allLines = Files.lines(realPath);
        allLines.forEach(LOGGER::debug);
        int lines = (int) Files.lines(realPath).map(x -> x.split(",")).count();
        LOGGER.debug("Size after splitting with ',' " + lines);
        TestCase.assertTrue(lines > 100);
    }

    /**
     * Check if the file produced is not null and make sure that all the process works
     */
    @Test
    public void readInputMobyDick() {
        String path = this.getClass().getClassLoader().getResource("Moby_Dick.txt").getPath();
        LOGGER.debug("Path: " + path);
        mapReduce.setAbsoluteInputPath(path);
        mapReduce.start();
        TestCase.assertNotNull(this.getClass().getClassLoader().getResource("Moby_Dick.txt.csv"));
    }

    @Test
    public void readInputMobyDickCountResult() throws IOException {
        String path = this.getClass().getClassLoader().getResource("Moby_Dick.txt").getPath();
        LOGGER.debug("Path: " + path);
        mapReduce.setAbsoluteInputPath(path);
        mapReduce.start();
        TestCase.assertNotNull(this.getClass().getClassLoader().getResource("Moby_Dick.txt.csv"));
        path = this.getClass().getClassLoader().getResource("Moby_Dick.txt.csv").getPath();
        Path realPath = Path.of(path);
        var allLines = Files.lines(realPath);
        allLines.forEach(LOGGER::debug);
        int lines = (int) Files.lines(realPath).map(x -> x.split(",")).count();
        LOGGER.debug("Size after splitting with ',' " + lines);
        TestCase.assertTrue(lines > 100);
    }

    /**
     * Check if the file produced is not null and make sure that all the process works
     */
    @Test
    public void readInputAdventuresOfSherlockHolmes() {
        String path = this.getClass().getClassLoader().getResource("The_Adventures_of_Tom_Sawyer.txt").getPath();
        LOGGER.debug("Path: " + path);
        mapReduce.setAbsoluteInputPath(path);
        mapReduce.start();
        TestCase.assertNotNull(this.getClass().getClassLoader().getResource("The_Adventures_of_Tom_Sawyer.txt.csv"));
    }

    @Test
    public void readInputTheAdventuresOfSherlockHolmesCountResult() throws IOException {
        String path = this.getClass().getClassLoader().getResource("The_Adventures_of_Sherlock_Holmes.txt").getPath();
        LOGGER.debug("Path: " + path);
        mapReduce.setAbsoluteInputPath(path);
        mapReduce.start();
        TestCase.assertNotNull(this.getClass().getClassLoader().getResource("The_Adventures_of_Sherlock_Holmes.txt.csv"));
        path = this.getClass().getClassLoader().getResource("The_Adventures_of_Sherlock_Holmes.txt.csv").getPath();
        Path realPath = Path.of(path);
        var allLines = Files.lines(realPath);
        allLines.forEach(LOGGER::debug);
        int lines = (int) Files.lines(realPath).map(x -> x.split(",")).count();
        LOGGER.debug("Size after splitting with ',' " + lines);
        TestCase.assertTrue(lines > 100);
    }

    /**
     * Check if the file produced is not null and make sure that all the process works
     */
    @Test
    public void readInputTheAdventuresOfTomSawyer() {
        String path = this.getClass().getClassLoader().getResource("The_Adventures_of_Tom_Sawyer.txt").getPath();
        LOGGER.debug("Path: " + path);
        mapReduce.setAbsoluteInputPath(path);
        mapReduce.start();
        TestCase.assertNotNull(this.getClass().getClassLoader().getResource("The_Adventures_of_Tom_Sawyer.txt.csv"));
    }

    @Test
    public void readInputTheAdventuresOfTomSawyerCountResult() throws IOException {
        String path = this.getClass().getClassLoader().getResource("The_Adventures_of_Tom_Sawyer.txt").getPath();
        LOGGER.debug("Path: " + path);
        mapReduce.setAbsoluteInputPath(path);
        mapReduce.start();
        TestCase.assertNotNull(this.getClass().getClassLoader().getResource("The_Adventures_of_Tom_Sawyer.txt.csv"));
        path = this.getClass().getClassLoader().getResource("The_Adventures_of_Tom_Sawyer.txt.csv").getPath();
        Path realPath = Path.of(path);
        var allLines = Files.lines(realPath);
        allLines.forEach(LOGGER::debug);
        int lines = (int) Files.lines(realPath).map(x -> x.split(",")).count();
        LOGGER.debug("Size after splitting with ',' " + lines);
        TestCase.assertTrue(lines > 100);
    }

    @Test
    public void readInputUlysses() {
        String path = this.getClass().getClassLoader().getResource("Ulysses.txt").getPath();
        LOGGER.debug("Path: " + path);
        mapReduce.setAbsoluteInputPath(path);
        mapReduce.start();
        TestCase.assertNotNull(this.getClass().getClassLoader().getResource("Ulysses.txt.csv"));
    }

    @Test
    public void readInputUlyssesCountResult() throws IOException {
        String path = this.getClass().getClassLoader().getResource("Ulysses.txt").getPath();
        LOGGER.debug("Path: " + path);
        mapReduce.setAbsoluteInputPath(path);
        mapReduce.start();
        TestCase.assertNotNull(this.getClass().getClassLoader().getResource("Ulysses.txt.csv"));
        path = this.getClass().getClassLoader().getResource("Ulysses.txt.csv").getPath();
        Path realPath = Path.of(path);
        var allLines = Files.lines(realPath);
        allLines.forEach(LOGGER::debug);
        int lines = (int) Files.lines(realPath).map(x -> x.split(",")).count();
        LOGGER.debug("Size after splitting with ',' " + lines);
        TestCase.assertTrue(lines > 100);
    }
}
