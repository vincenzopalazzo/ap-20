module Utils where

cp_log :: (Show a) => [a] -> String

cp_log [] = ""
cp_log [x] = (show x)
cp_log (x:xs) = (show x) ++ ", " ++ cp_log xs
