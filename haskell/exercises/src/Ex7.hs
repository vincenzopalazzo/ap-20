module Ex7 where

isPalindromic :: String -> Bool

isPalindromic string = string == (reverse string)

hasVowels :: String -> Int

isMember :: Char -> String -> Bool

isMember elem [] = False
isMember elem (x:xs)
  | elem == x  = True
  | otherwise = isMember elem xs

-- aeiou
hasVowels string = _hasVowels string vowels where
  _hasVowels [] _ = 0
  _hasVowels (x:xs) vowels
    | (isMember x vowels) = 1 + (_hasVowels xs vowels)
    | otherwise = 0 + (_hasVowels xs vowels)
  vowels = "aeiou"

countVowelPaliR :: [String] -> Int

countVowelPaliR list = _countVowelPaliR list where
  _countVowelPaliR [] = 0
  _countVowelPaliR (x:xs) =
    if (isPalindromic x) then (hasVowels x) + (_countVowelPaliR xs)
    else 0 + (_countVowelPaliR xs)


countVowelPaliC :: [String] -> Int

countVowelPaliC listString =
  foldl (+) 0 (map (\x -> if isPalindromic x then hasVowels x else 0) listString)
