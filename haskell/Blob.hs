import Data.Char (toUpper)

-- Es 1
myReplicateR 1 x = [x]
myReplicateR a x = [x] ++ (myReplicateR (a - 1) x)


myReplicateC a b = map (\_ -> b) [1..a]

-- Es 2
sumOddR :: (Integral  a) => [a] -> a

sumOddR [x] = if odd x then x else 0
sumOddR (x:xs) = (if (odd x) then x else 0) + (sumOddR xs)

sumOddC :: (Integral a) => [a] -> a
sumOddC xs = foldl (+) 0 (filter (odd) xs)

-- Es3
replR [] n = []
replR (x:xs) n = (myReplicateR n x) ++ (replR xs n)

replC :: (Show a, Integral n) => [a] -> n -> [a]
replC list n =
  foldl (++) [] (map (\x -> (myReplicateC n x)) list)

-- Es4
totalLengthR [] = 0
totalLengthR (x:xs) =
  (if (head x) == 'A' then Prelude.length x else 0) + totalLengthR xs

totalLengthC list =
  foldl (+) 0 (map(\x -> (if (head x) == 'A' then Prelude.length x else 0)) list)

-- Es5
filterOddR :: (Show a) => [a] -> [a]

filterOddR [] = []
filterOddR list = _filterOddR list 0 where
  _filterOddR [] _ = []
  _filterOddR (x:xs) index =
        if (odd index) then []  ++ (_filterOddR xs (index + 1))
        else [x] ++ (_filterOddR xs (index + 1))

filterOddC :: (Show a) => [a] -> [a]

filterOddC list =
  map (fst $) (filter (\x -> (even (snd x))) (zip list [0..(length list)]))

-- Es6
toCapital :: String -> String

toCapital [] = []
toCapital (x:xs) = toUpper (x) : cycle xs where
  cycle [] = []
  cycle (x:xs) = x : cycle xs

--titleCaseR :: String -> String

titleCaseR string = unwords(_titleCaseR (words string)) where
  _titleCaseR [] = []
  _titleCaseR (x:xs) = toCapital x : _titleCaseR xs

--titleCaseC :: String -> String
titleCaseC string =
  unwords(map toCapital (words string ))

-- Es7
isPalindromic :: String -> Bool

isPalindromic string = string == (reverse string)

hasVowels :: String -> Int

isMember :: Char -> String -> Bool

isMember elem [] = False
isMember elem (x:xs)
  | elem == x  = True
  | otherwise = isMember elem xs

-- aeiou
hasVowels string = _hasVowels string vowels where
  _hasVowels [] _ = 0
  _hasVowels (x:xs) vowels
    | (isMember x vowels) = 1 + (_hasVowels xs vowels)
    | otherwise = 0 + (_hasVowels xs vowels)
  vowels = "aeiou"

countVowelPaliR :: [String] -> Int

countVowelPaliR list = _countVowelPaliR list where
  _countVowelPaliR [] = 0
  _countVowelPaliR (x:xs) =
    if (isPalindromic x) then (hasVowels x) + (_countVowelPaliR xs)
    else 0 + (_countVowelPaliR xs)


countVowelPaliC :: [String] -> Int

countVowelPaliC listString =
  foldl (+) 0 (map (\x -> if isPalindromic x then hasVowels x else 0) listString)

-- Es8

map' f xs = foldr (\x ys -> f x : ys) [] xs

-- Es9
data IntTree = Leaf Int | Node (Int, IntTree, IntTree)

--tmap :: func -> tree -> tree
tmap func (Leaf val) = Leaf (func val)
tmap func (Node(val, left, right)) =
  Node ((func val), tmap func left, tmap func right)

succTree tree =
  tmap succ tree

-- sumSucc :: (IntTree tree) => tree -> Int
sumSucc tree = _sumSucc (succTree tree) where
  _sumSucc (Leaf (val)) = val
  _sumSucc (Node (val, leaft, right)) =
    val + (_sumSucc leaft) + (_sumSucc right)