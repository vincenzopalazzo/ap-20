plugins {
    application
}

repositories {
    jcenter()
}

dependencies {
    implementation(files("devlib/mapreduce.jar"))
    implementation("info.picocli:picocli:4.6.1")

    testImplementation("ch.qos.logback:logback-classic:1.2.3")
    testImplementation("ch.qos.logback:logback-core:1.2.3")
    testImplementation("org.slf4j:slf4j-api:1.7.25")
    testImplementation("junit:junit:4.13")
}

application {
    mainClass.set("it.unipi.vincenzopalazzo.ex4.App")
}

tasks {
    register("fatJar", Jar::class.java) {
        archiveClassifier.set("all")
        duplicatesStrategy = DuplicatesStrategy.EXCLUDE
        manifest {
            attributes("Main-Class" to application.mainClass)
        }
        from(
            configurations.runtimeClasspath.get()
                .onEach { println("add from dependencies: ${it.name}") }
                .map { if (it.isDirectory) it else zipTree(it) }
        )
        val sourcesMain = sourceSets.main.get()
        sourcesMain.allSource.forEach { println("add from sources: ${it.name}") }
        from(sourcesMain.output)
    }
}
