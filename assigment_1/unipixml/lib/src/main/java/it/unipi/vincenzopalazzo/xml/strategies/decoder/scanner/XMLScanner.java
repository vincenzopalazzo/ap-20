/*
 * Advanced Programming project 2020/2021
 * <p>
 * Copyright (C) 2020-2021 Vincenzo Palazzo vincenzopalazzodev@gmail.com
 * and all professors of the Advanced Programming class
 * http://pages.di.unipi.it/corradini/Didattica/AP-20/
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package it.unipi.vincenzopalazzo.xml.strategies.decoder.scanner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is a Scanner implementation to tokenize the XML source in a list of TOKEN and pass to the
 * the parser that create a AST/DOM.
 *
 * @author Vincenzo Palazzo v.palazzo1@studenti.unipi.it
 */
public class XMLScanner {

  private static Logger LOGGER = LoggerFactory.getLogger(XMLScanner.class);
  private static HashMap<String, TokenType> tokens = new HashMap<>();

  // Define the keywords of the XML Syntax.
  static {
    tokens.put("type", TokenType.TYPE_ELEM);
    tokens.put("</", TokenType.CLOSE_TAG);
    tokens.put("<", TokenType.OPEN_TAG);
    tokens.put(">", TokenType.END_TAG);
  }

  // All the object that referer from the source
  // and the position in the file
  private int start;
  private int current;
  private int line;
  private String source;
  // The list of token created by the source
  private List<Token> tokensList;

  public XMLScanner(String source) {
    this.source = source;
    this.start = 0;
    this.line = 0;
    this.current = 0;
    this.tokensList = new ArrayList<>();
  }

  // The main method to run the scanner on the source content
  public List<Token> scanTokens() {
    while (!isFinished()) {
      start = current; // Update the pointer
      scanToken(); // read the next token
    }
    tokensList.add(new Token(current, TokenType.EOF, null, null));
    return tokensList;
  }

  // Tokenize the char and detect what is the correct token
  // TODO: Parse the XML comments.
  private void scanToken() {
    char valueNow = this.next();
    switch (valueNow) {
      case '\n':
        {
          line++;
        }
      case '\t':
        {
          /*consume*/
          break;
        }
      case 'r':
        {
          /*consume*/
          break;
        }
      case ' ':
        {
          /*consume*/
          break;
        }
      case '=':
        {
          this.addToken(TokenType.EQ, null);
          break;
        }
      case '<':
        {
          // remove ambiguity
          if (isNext('/')) {
            // next();
            addToken(TokenType.CLOSE_TAG, null);
            // parseContentTag('>', TokenType.NAME_TAG, 2);
          } else if (isNext('?')) {
            // reade the file until the ?> end
            while (peek() != '?' && !isNext('>')) next();
            next(); // consume the ">" token
            current++;
            // addToken(TokenType.XML_DEC, null);
          } else {
            addToken(TokenType.OPEN_TAG, null);
            // parseContentTag('>', TokenType.NAME_TAG, 1);
          }
          break;
        }
      case '>':
        {
          addToken(TokenType.END_TAG, null);
          break;
        }
      case '"':
        {
          parseString();
          break;
        }
      default:
        {
          // The default case of this statment can be dangerus because
          // it can hide some parser bug, for this reason, this block will
          // contains mode log statment
          LOGGER.debug("This should be the content of the tag");
          // parseContentTag('<', TokenType.VALUE_TAG, 0);
          parseIdentifier();
          break;
        }
    }
  }

  // This method consider the name of the tag with the same type of
  // the type of the value of the tag.
  private void parseIdentifier() {
    while (isAlphaNumeric(peek())) next();
    String value = source.substring(start, current);
    TokenType type = TokenType.NAME_TAG;
    if (tokens.containsKey(value)) {
      type = tokens.get(value);
      value = null; // found a keyword, I don't care of the value
      LOGGER.debug("Tokens found: " + type);
    }
    LOGGER.debug(
        "Last value added before to add the next: "
            + tokensList.get(tokensList.size() - 1).getType());
    if (type == TokenType.NAME_TAG && isPrev(TokenType.END_TAG)) type = TokenType.VALUE_TAG;
    LOGGER.debug("Indentifier found with type of " + type + " and value " + value);
    addToken(type, value);
  }

  /** This method contains the logic to pars a string contained between " .. " */
  private void parseString() {
    while (!isFinished() && !isNext('"')) next();
    String type = source.substring(start, current);
    LOGGER.debug("The type value is: " + type);
    addToken(TokenType.TYPE_VALUE, type);
  }

  /**
   * Get the actual character in the source stream
   *
   * @return Return the character in th current position
   */
  private char peek() {
    return isFinished() ? '\u0000' : source.charAt(current);
  }

  /**
   * This method check if the character is a digit or a letter.
   *
   * @param target The character where the scanner need the information
   * @return Return true if it is alpha numeric
   */
  private boolean isAlphaNumeric(char target) {
    return Character.isAlphabetic(target) || Character.isDigit(target);
  }

  /**
   * Advance into the stream
   *
   * @return return the previous character
   */
  private char next() {
    current++;
    return source.charAt(current - 1); // indexed by 0
  }

  /**
   * Check if the next char in the stream s the expected character
   *
   * @param expected expected char
   * @return Return true if the expected char is the next value in the strea,
   */
  private boolean isNext(char expected) {
    if (isFinished()) return false;
    if (source.charAt(current) != expected) return false;
    current++;
    return true;
  }

  /**
   * Check if the previous character has the TokenType expected
   *
   * @param expected TokenType expected
   * @return return a true is the character match with the content.
   */
  private boolean isPrev(TokenType expected) {
    if (tokensList.isEmpty()) return false;
    return tokensList.get(tokensList.size() - 1).getType() == expected;
  }

  /**
   * Add the token at the list
   *
   * @param tokenType Token type of the token to add
   * @param value The value of the token (if any)
   */
  private void addToken(TokenType tokenType, String value) {
    String tokenValue = source.substring(start, current);
    tokensList.add(new Token(current, tokenType, tokenValue, value));
  }

  /**
   * Check if we reach the end of the stream
   *
   * @return Return true id the stream is ended.
   */
  private boolean isFinished() {
    return this.current >= source.length();
  }
}
