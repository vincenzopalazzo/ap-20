/*
 * Advanced Programming project 2020/2021
 * <p>
 * Copyright (C) 2020-2021 Vincenzo Palazzo vincenzopalazzodev@gmail.com
 * and all professors of the Advanced Programming class
 * http://pages.di.unipi.it/corradini/Didattica/AP-20/
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package it.unipi.vincenzopalazzo.xml.strategies.decoder;

import it.unipi.vincenzopalazzo.xml.strategies.decoder.ast.INode;
import it.unipi.vincenzopalazzo.xml.strategies.decoder.parser.XMLParser;
import it.unipi.vincenzopalazzo.xml.strategies.decoder.scanner.Token;
import it.unipi.vincenzopalazzo.xml.strategies.decoder.scanner.XMLScanner;
import it.unipi.vincenzopalazzo.xml.strategies.decoder.strategy.IDecodeStrategy;
import it.unipi.vincenzopalazzo.xml.strategies.decoder.strategy.ListStrategy;
import it.unipi.vincenzopalazzo.xml.strategies.decoder.strategy.ObjectStrategy;
import java.util.List;

/**
 * XML decoder is the Class that implement the decode operation from a XML content as String
 *
 * @author Vincenzo Palazzo v.palazzo1@studenti.unipi.it
 */
public class XMLDecoder {

  /**
   * This method take the content of the XML as String and the Class target as parameter and produce
   * the Object of type of the target as result.
   *
   * @param content The content of XML as String
   * @param target The class Target where the XML need to by bind
   * @param <T> The return type, that in this case is the same of the target as parameter
   * @return The Java Object that reflect the XML file, if the decode operation is possible.
   */
  public static <T> T deserialize(String content, Class target) {
    INode ast = parseContent(content);
    IDecodeStrategy strategy = getCorrectStrategy(target, false);
    return (T) strategy.decode(ast);
  }

  /**
   * This method take the content of the XML as String and the Class target as parameter and produce
   * the list of Java object of type of the target as result.
   *
   * @param content The content of XML as String
   * @param target The class Target where the XML need to by bind
   * @param <T> The return type, that in this case is the same of the target as parameter
   * @param isCollection The flag that said is the XML contains a collection of elements
   * @return The list of Java Object that reflect the XML file, if the decode operation is possible.
   */
  public static <T> List<T> deserialize(String content, Class target, boolean isCollection) {
    INode ast = parseContent(content);
    IDecodeStrategy strategy = getCorrectStrategy(target, isCollection);
    return (List<T>) strategy.decode(ast);
  }

  /**
   * Helper method that contains the logic to produce the AST or DOM of the XML document as a Tree.
   *
   * @param content XML content as String
   * @return Return the DOM of XML as Tree
   */
  private static INode parseContent(String content) {
    XMLScanner scanner = new XMLScanner(content);
    List<Token> tokens = scanner.scanTokens();
    XMLParser parser = new XMLParser(tokens);
    INode ast = parser.parser();
    return ast;
  }

  /**
   * Helper method to decide what is the decode strategy to use with the information as parameter
   *
   * @param target The class target where map the Java Object
   * @param isCollection A flag that give information is the XML contains a collection of Object
   * @param <T> The return Type where the Strategy will be mapped
   * @return Return the correct Strategy for the fiven information.
   */
  private static <T> IDecodeStrategy getCorrectStrategy(Class target, boolean isCollection) {
    if (isCollection) {
      ListStrategy<List<T>> strategy = new ListStrategy<>(target);
      return strategy;
    }
    ObjectStrategy<T> strategy = new ObjectStrategy<>(target);
    return strategy;
  }
}
