package it.unipi.vincenzopalazzo.xml;

import it.unipi.vincenzopalazzo.xml.mock.Student;
import it.unipi.vincenzopalazzo.xml.strategies.decoder.ast.INode;
import it.unipi.vincenzopalazzo.xml.strategies.decoder.parser.XMLParser;
import it.unipi.vincenzopalazzo.xml.strategies.decoder.scanner.Token;
import it.unipi.vincenzopalazzo.xml.strategies.decoder.scanner.XMLScanner;
import it.unipi.vincenzopalazzo.xml.strategies.decoder.strategy.IDecodeStrategy;
import it.unipi.vincenzopalazzo.xml.strategies.decoder.strategy.ListStrategy;
import it.unipi.vincenzopalazzo.xml.strategies.decoder.strategy.ObjectStrategy;
import java.util.ArrayList;
import java.util.List;
import junit.framework.TestCase;
import org.junit.Test;

public class XMLDecoderTest {

  @Test
  public void decoderSimpleObject() {
    ObjectStrategy<Student> decoderStrategy = new ObjectStrategy<>(Student.class);
    String portionXml =
        "<Student>\n"
            + "\t<firstName type=\"String\">Vincenzo</firstName>\n"
            + "\t<age type=\"int\">24</age>\n"
            + "</Student>";
    XMLScanner scanner = new XMLScanner(portionXml);
    List<Token> tokens = scanner.scanTokens();
    XMLParser parser = new XMLParser(tokens);
    INode ast = parser.parser();
    Student student = decoderStrategy.decode(ast);
    TestCase.assertEquals(student.getFirstName(), "Vincenzo");
  }

  @Test
  public void decoderListOfObject() {
    IDecodeStrategy<List<Student>> decoder = new ListStrategy<>(Student.class);
    String portionXml =
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
            + "<Students>\n"
            + "<Student>\n"
            + "\t<firstName type=\"String\">Vincenzo</firstName>\n"
            + "\t<age type=\"int\">24</age>\n"
            + "</Student>\n"
            + "<Student>\n"
            + "\t<firstName type=\"String\">Vincenzo</firstName>\n"
            + "\t<age type=\"int\">24</age>\n"
            + "</Student>\n"
            + "</Students>";
    XMLScanner scanner = new XMLScanner(portionXml);
    List<Token> tokens = scanner.scanTokens();
    XMLParser parser = new XMLParser(tokens);
    INode ast = parser.parser();
    List<Student> students = decoder.decode(ast);
    TestCase.assertFalse(students.isEmpty());
    TestCase.assertEquals(2, students.size());
    Student first = students.get(0);
    Student second = students.get(1);
    TestCase.assertEquals(first, second);
  }

  @Test
  public void decodeWithFacade() {
    JXMLFacade jxml = new JXMLFacade();
    Student student = new Student("Vincenzo", "Palazzo", 24, "3B");
    List<Student> students = new ArrayList<>();
    students.add(student);
    jxml.toXml(students.toArray(), "array");
    students = null;
    TestCase.assertNull(students);
    String loadXML = jxml.loadContentFile("array.xml");
    students = jxml.fromXml(loadXML, Student.class, true);
    TestCase.assertNotNull(students);
    TestCase.assertEquals(1, students.size());
  }
}
