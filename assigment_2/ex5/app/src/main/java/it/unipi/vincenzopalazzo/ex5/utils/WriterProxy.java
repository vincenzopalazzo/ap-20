/**
 * Advanced Programming project 2020/2021
 * <p>
 * Copyright (C) 2020-2021 Vincenzo Palazzo vincenzopalazzodev@gmail.com
 * and all professors of the Advanced Programming class
 * http://pages.di.unipi.it/corradini/Didattica/AP-20/
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package it.unipi.vincenzopalazzo.ex5.utils;

import it.unipi.vincenzopalazzo.mapreduce.utils.Pair;
import it.unipi.vincenzopalazzo.mapreduce.utils.Writer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.stream.Stream;

/*
 * This class is used to write the result on a disk, it extend the writer only
 * to respect the a generic interface given by the map reduce. This is not a proxy
 * patter, but the name proxy derive from a idea that this calls is not the real Writer object from the
 * mapreduce lib but it is another one.
 * the writer is extend only to give a reader of the code the understanding that this is the same concept but
 * I need an different method to save the info.
 *
 * @author Vincenzo Palazzo <v.palazzo1@studenti.unipi.it>
 */
public class WriterProxy extends Writer {
    public static void writeInvertedIndex(File dst, Stream<Pair<String, Pair<String, Integer>>> res) throws FileNotFoundException {
        PrintStream ps = new PrintStream(dst);
        res.forEach(p -> ps.println(p.getKey() + ", " + p.getValue().getKey() + ", " + p.getValue().getValue()));
        ps.close();
    }
}
