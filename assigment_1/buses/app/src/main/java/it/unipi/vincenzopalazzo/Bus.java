/**
 * Advanced Programming project 2020/2021
 * <p>
 * Copyright (C) 2020-2021 Vincenzo Palazzo vincenzopalazzodev@gmail.com
 * and all professors of the Advanced Programming class
 * http://pages.di.unipi.it/corradini/Didattica/AP-20/
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

package it.unipi.vincenzopalazzo;

import it.unipi.vincenzopalazzo.exceptions.BusException;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.io.Serializable;
import java.util.Date;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Bus model of the Busses at the time of Covid pandemic Application.
 *
 * @author Vincenzo Palazzo <v.palazzo1@studenti.unipi.it>
 */
public class Bus implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(Bus.class);
    public static final String DOOR_OPEN = "DOOR_OPEN";
    public static final String NUM_PASSENGERS = "NUM_PASSENGERS";

    /**
     * Capacity of the bus, by default the value is 50
     */
    private int capacity;
    /**
     * Flag that mean that the dor of the bus is open,
     * by default it is false
     */
    private boolean doorOpen;
    /**
     * Num passengers is bound and constraint because it is not possible have more people that
     * the maximum capacity. Initial value is 20
     */
    private int numPassengers;
    /**
     * Timer to decrement the number of passengers every
     * few second
     */
    private Timer passengerTimer;
    /**
     * Timer to close the dore when it is was open
     * from the user action
     */
    private Timer doorTimer;

    /**
     * Propriety change listener to notify the change of the door status
     */
    private PropertyChangeSupport nofityChange;
    /**
     * Vetoable change listener, used to notify when a change
     * to the propriety  numPassengers is accepted or is rejected
     */
    private VetoableChangeSupport notifyVetoable;

    /**
     * Default and unique constructor of the class,
     * after the call of this constructor all the default propriety are bind
     * to the default value and all the listener are bind to the instance of the class.
     * <p>
     * In addition, the timer to decrement the passenger number is called.
     */
    public Bus() {
        this.capacity = 50;
        this.doorOpen = false;
        this.numPassengers = 20;
        this.nofityChange = new PropertyChangeSupport(this);
        this.notifyVetoable = new VetoableChangeSupport(this);
        // The covid controlled is set inside the Bus class
        this.addVetoableChangeListener(new CovidController());
        //this.activated();
    }

    public int getCapacity() {
        return capacity;
    }

    public boolean isDoorOpen() {
        return doorOpen;
    }

    public int getNumPassengers() {
        return numPassengers;
    }

    /**
     * The method setNumPassengers run the logic to set a new number of passenger
     * if it is possible, otherwise an Runtime exception is generated.
     *
     * This method i synchronized because it is called also from the Timer and is needed to manager
     * the race condition.
     *
     * @param numPassengers The number of passenger that are entering inside the bug
     * @throws BusException The runtime exception generated when the total number of passenger are too much for the total capacity
     */
    public synchronized void setNumPassengers(int numPassengers) throws BusException {
        if (isValidNumPassengers(numPassengers)) {
            try {
                int newValue = this.numPassengers + numPassengers;
                int oldVal = this.numPassengers;
                this.notifyVetoable.fireVetoableChange(CovidController.NUM_PASSENGER, oldVal, newValue);
                this.setDoorOpen(true);
                this.numPassengers = newValue;
                this.nofityChange.firePropertyChange(NUM_PASSENGERS, oldVal, this.numPassengers);
                this.closeDoorAfterEvent();
                LOGGER.debug("Operation setNumPassengers ACCEPTED");
            } catch (PropertyVetoException ex) {
                LOGGER.error("Number of passenger rejected");
                throw new BusException(ex.getLocalizedMessage(), ex.getCause());
            } finally {
                this.activated();
            }
        }
    }

    /**
     * The method setDoorOpen is the unique method to set the status of the bus door.
     * In addition this method is used also to notify all the listener register to the
     * propriety change listener nofityChange
     * @param doorOpen The new status of the door, true mean that the door is open, false otherwise.
     */
    public void setDoorOpen(boolean doorOpen) {
        boolean old = this.doorOpen;
        this.doorOpen = doorOpen;
        this.nofityChange.firePropertyChange(DOOR_OPEN, old, this.doorOpen);
    }

    public void addProprietyListener(PropertyChangeListener changeListener) {
        if (changeListener == null)
            throw new IllegalArgumentException("PropertyChangeListener null");
        this.nofityChange.addPropertyChangeListener(changeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener changeListener) {
        if (changeListener == null)
            throw new IllegalArgumentException("PropertyChangeListener null");
        this.nofityChange.removePropertyChangeListener(changeListener);
    }

    public void addVetoableChangeListener(VetoableChangeListener vetoListener) {
        if (vetoListener == null)
            throw new IllegalArgumentException("PropertyChangeListener null");
        this.notifyVetoable.addVetoableChangeListener(vetoListener);
    }

    public void removeVetoableChangeListener(VetoableChangeListener vetoListener) {
        if (vetoListener == null)
            throw new IllegalArgumentException("PropertyChangeListener null");
        this.notifyVetoable.removeVetoableChangeListener(vetoListener);
    }

    /**
     * This function is called inside the timer each 5 second
     * to giver the possibility to the passenger to exit from the bus.
     */
    private synchronized void exitPassenger() {
        if (this.numPassengers > 0) {
            //TODO debug this functions
            var randomNum = new Random().nextInt(this.numPassengers);
            LOGGER.info("Random value is: " + randomNum);
            var oldValue = this.numPassengers;
            var newValue = Math.max(0, oldValue - randomNum);
            try {
                // This could be avoided but I want make sure that the random number don't make
                // my calculation negative.
                this.notifyVetoable.fireVetoableChange(CovidController.NUM_PASSENGER, oldValue, newValue);
                this.setDoorOpen(true);
                LOGGER.info("Decrease passenger of value: " + newValue);
                this.numPassengers = newValue;
               this.nofityChange.firePropertyChange(NUM_PASSENGERS, oldValue, newValue);
               this.closeDoorAfterEvent();
            } catch (PropertyVetoException ex) {
                // This should be never happen
                LOGGER.error("Error produced in the listener " + ex.getLocalizedMessage());
            }
        }
    }

    /**
     * The method closeDoorAfterEvent is the method called from the setNumPassengers
     * to close the dor after x second. If the timer is running when this method is called
     * the design choice is to reset the timer and give another 5 seconds.
     */
    private void closeDoorAfterEvent() {
        if (doorTimer != null)
            doorTimer.cancel();
        this.doorTimer = new Timer();
        this.doorTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                setDoorOpen(false);
                doorTimer.cancel();
                LOGGER.info("Close door after timer end");
            }
        }, 2000);
    }

    /**
     * This is a util method that help to collect the logic inside only one method
     * to check if the number of passenger are valid for the current rules
     * @param numPassengers The number of passengers that need to be verify
     * @return return a boolean value, true is the value is a valid value, false otherwise.
     */
    private boolean isValidNumPassengers(int numPassengers) {
        int futureSum = numPassengers + this.numPassengers;
        if (futureSum <= this.capacity) {
            return true;
        }
        return false;
    }

    /**
     * When this method is called, it exploit a timer and numPassengers
     * decreased randomly every few seconds (but never becomes negative).
     * <p>
     * The implementation set up the a timer to decrease randomly each
     * 5 second the number of passenger.
     * <p>
     * Each 5 second because we suppose that to exit from the buss a people need
     * 5 seconds.
     */
    public void activated() {
        if (this.passengerTimer != null)
            return;
        this.passengerTimer = new Timer();
        this.passengerTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                exitPassenger();
                LOGGER.debug("Decrease passenger number, at the moment there are " + numPassengers + " on bus");
            }

        }, new Date(), 5000);
    }
}
