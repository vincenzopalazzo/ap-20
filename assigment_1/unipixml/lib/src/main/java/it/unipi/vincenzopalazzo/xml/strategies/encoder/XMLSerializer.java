/*
 * Advanced Programming project 2020/2021
 * <p>
 * Copyright (C) 2020-2021 Vincenzo Palazzo vincenzopalazzodev@gmail.com
 * and all professors of the Advanced Programming class
 * http://pages.di.unipi.it/corradini/Didattica/AP-20/
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package it.unipi.vincenzopalazzo.xml.strategies.encoder;

import it.unipi.vincenzopalazzo.xml.JXMLException;
import it.unipi.vincenzopalazzo.xml.annotations.XMLabel;
import it.unipi.vincenzopalazzo.xml.annotations.XMLfield;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * XMLSerialize is the class required from the assignment, but is it develope to be used inside the
 * JXMLFacade to make the Interface of the small toy library more simile to the real library like
 * Jackson and so on.
 *
 * @author Vincenzo Palazzo v.palazzo1@studenti.unipi.it
 */
public class XMLSerializer {

  private static final Logger LOGGER = LoggerFactory.getLogger(XMLSerializer.class);

  /**
   * This method is used to serialize a list of object and this is considered the main functionality
   * of the "library". This method is required by the Assignment and it is used in the HXMLFacade
   * Class interface of the library an example of usage is inside the test directory
   *
   * @param arr the array of object to serialize
   * @param fileName the file name create in the same directory where the program is ran
   */
  public static void serialize(Object[] arr, String fileName) {
    if (fileName == null || fileName.isEmpty()) {
      LOGGER.error("File name is not valid");
      throw new JXMLException("Illegal file name");
    }
    PrintWriter writer = null;
    try {
      writer = new PrintWriter(fileName.concat(".xml"), StandardCharsets.UTF_8);
      // XML prolog
      writer.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
      if (arr.length == 0) return;
      writer.println("<" + arr[0].getClass().getSimpleName() + "s>");
      for (Object o : arr) {
        String result = XMLSerializer.serialize(o);
        writer.println(result);
        writer.flush();
      }
      writer.println("</" + arr[0].getClass().getSimpleName() + "s>");
    } catch (Exception e) {
      throw new JXMLException(e.getCause());
    } finally {
      if (writer != null) writer.close();
    }
  }

  /**
   * This method is a helper method used to make the incremental testing on the library, and also to
   * divide the logic in different methods.
   *
   * @param value The object to serialize in XML
   * @return The string is the result of the serialization in the XML format
   */
  public static String serialize(Object value) throws JXMLException {
    if (value == null) {
      LOGGER.error("Argument function null");
      throw new IllegalArgumentException("Argument function null");
    }
    if (!value.getClass().isAnnotationPresent(XMLabel.class)) {
      LOGGER.debug("Annotation XMLable not present on the object");
      return null;
    }
    StringBuilder resultBuilder = new StringBuilder();
    List<Field> hierarchyFiled = new ArrayList<>();
    Class className = value.getClass();
    // TODO there is a better way?
    // Going up in the hierarchy to search if there are info to deserialize
    while (className != null) {
      hierarchyFiled.addAll(Arrays.asList(className.getDeclaredFields()));
      className = className.getSuperclass();
    }
    int fieldsSize = hierarchyFiled.size();
    LOGGER.debug("Starting");
    LOGGER.debug("Number of fields: " + fieldsSize);
    if (fieldsSize == 0) return null;
    resultBuilder.append(String.format("<%s>", value.getClass().getSimpleName())).append("\n");
    for (Field field : hierarchyFiled) {
      LOGGER.debug("Analyzing the filed with name " + field.getName());
      if (field.isAnnotationPresent(XMLfield.class)) {
        XMLfield annotation = field.getAnnotation(XMLfield.class);
        if (annotation.name().trim().isEmpty()) {
          try {
            field.setAccessible(true);
            // Encoding <FiledName
            resultBuilder.append("\t").append("<").append(field.getName());
            // Encoding type="typeName">
            resultBuilder
                .append(" type=\"")
                .append(field.getType().getSimpleName())
                .append("\"")
                .append(">");
            // Encoding value</filedName>
            resultBuilder
                .append(field.get(value))
                .append("</")
                .append(field.getName())
                .append(">")
                .append("\n");
            field.setAccessible(false); // Good manners
            LOGGER.debug(
                String.format(
                    "Type of filed %s is %s", field.getName(), field.getType().getSimpleName()));
          } catch (IllegalAccessException e) {
            throw new JXMLException(e.getCause());
          }
        }
      }
    }
    resultBuilder.append(String.format("</%s>", value.getClass().getSimpleName()));
    LOGGER.debug("\n" + resultBuilder);
    return resultBuilder.toString();
  }
}
