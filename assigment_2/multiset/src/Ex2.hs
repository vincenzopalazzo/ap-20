module Ex2 (mapLB) where

import           Ex1 (ListBag (LB), fromList, insert, toList)

{-|
  This is a implementation of a Foldable class represents List Bag data structures
  that can be reduced to a summary value one element at a time (the multiplicity of an element is ignored).
-}
instance Foldable ListBag where
  {-|
   Right-associative fold of a structure, under the hood is implemented with the tail recursion
   that help to manage the performance with a data structure with a lot of elements.

   The variable name are, func = function, accumulator, bag is the container (ListBag) and
   the function used to implement the tail recursion have the same name with the _ as prefix.
  -}
  foldr func accumulator bag = _foldr func accumulator bag where
    _foldr func accumulator (LB []) = accumulator
    _foldr func accumulator (LB ((elm, _):xs)) = _foldr func (func elm accumulator) (LB xs)

{-|
 The function mapLB take a function and a container (ListBag) and return a new
 container (ListBag) where each element of the container have as value the result of
 function application.

This function can be implement easly with the function call fromList toList but, these function takes in consideration also the miltiplicity and this increase the complexity of the algorithm because the algorithm toList need to replicate each element for the multiplicity.

To improve the complexity of the algorithm is used the definition of Equality between multiset, that is said

"These objects are all different, when viewed as multisets, although they are the same set, since they all consist of the same elements. As with sets, and in contrast to tuples, order does not matter in discriminating multisets, so {a, a, b} and {a, b, a} denote the same multiset. To distinguish between sets and multisets, a notation that incorporates square brackets is sometimes used: the multiset {a, a, b} can be denoted as [a, a, b].[1]"

So to improve the complexity of the algorithm, I implement the insert function in List bag that use the operator : to append the element in a list in constant time, so the complexity of this algorithm is O(N). Basically each insert is create a new data structure.

The bad things of this function is that return a ListBag with a list of element in the reverse order, but this could be no matter for the previous definition of equality between multiset.
-}
mapLB :: Eq a => (a -> a) -> ListBag a -> ListBag a
mapLB func bag = _mapLB func bag (LB[]) where
  _mapLB func (LB [(elm, ml)]) newBag =
    insert newBag ((func elm), ml)
  _mapLB func (LB ((elm, ml):xs)) newBag =
    _mapLB func (LB xs) (insert newBag ((func elm), ml))

{-
 The implementation of mapLB was implemented as reported in the following
 comment that start with --
  However the complexity of the function fromList and toList is unknown because
 it depends from the multiplicity of each element and to make sure that the function mapLB
 is good enough the choise is to implement a insert operation that is insert with O(1) cost.
 With this solution is possible speedup the mapLB algorithm.
-}
--mapLB func z = fromList (map func(toList z))


{-
The following implementation of instance Functor ListBag return the following error at
compiler time

➜  multiset git:(master) ✗ make
ghc src/*.hs
[3 of 3] Compiling Ex2              ( src/Ex2.hs, src/Ex2.o )

src/Ex2.hs:48:10: error:
    • No instance for (Eq b) arising from a use of ‘mapLB’
      Possible fix:
        add (Eq b) to the context of
          the type signature for:
            fmap :: forall a b. (a -> b) -> ListBag a -> ListBag b
    • In the expression: mapLB
      In an equation for ‘fmap’: fmap = mapLB
      In the instance declaration for ‘Functor ListBag’

This mean that is not possible implement a instance of fmap with the
actual definition of ListBag.

The error is generated because the implementation of ListBag derive the Eq type.
In fact if the mapLB is implemented without Eq we receive the error from the insert method.
-}

-- uncomment this produce the error described above.
--instance Functor ListBag where
--  fmap = mapLB
