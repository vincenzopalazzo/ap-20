/*
 * Advanced Programming project 2020/2021
 * <p>
 * Copyright (C) 2020-2021 Vincenzo Palazzo vincenzopalazzodev@gmail.com
 * and all professors of the Advanced Programming class
 * http://pages.di.unipi.it/corradini/Didattica/AP-20/
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package it.unipi.vincenzopalazzo.xml;

import it.unipi.vincenzopalazzo.xml.strategies.decoder.XMLDecoder;
import it.unipi.vincenzopalazzo.xml.strategies.encoder.XMLSerializer;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

/**
 * JXMLFacade is the interface that fall in the optional part of the assignment, it is developer to
 * make a unique interface of the usage of the toy library.
 *
 * @author Vincenzo Palazzo v.palazzo1@studenti.unipi.it
 */
public class JXMLFacade {

  /**
   * This method under the hood used the XMLSerializer required by the assigment to create a file
   * with the "fileName" name and inside this file will serialize the array of objects in a XML file
   * well formed.
   *
   * @param arr The array of object to serialize
   * @param fileName the file name of the XML file where to save the serialization result.
   */
  public void toXml(Object[] arr, String fileName) {
    XMLSerializer.serialize(arr, fileName);
  }

  /**
   * This method is an additional method that fall in the option assignment and implement a
   * possibility to serialize a Object in a String, as the GSON library does.
   *
   * @param o Is a object to serialize into XML
   * @return The String that contains the XML form of the Object as parameter.
   */
  public String toXml(Object o) {
    return XMLSerializer.serialize(o);
  }

  /**
   * This method is an additional method that fall in the optional assigment, and give the
   * opportunity to loaf the file from the local machine and return the content as a String
   *
   * @param path The absolute path of the file that the user want to load
   * @return The content of the file as String
   */
  public String loadContentFile(String path) {
    try {
      String content = Files.readString(Path.of(path));
      return content;
    } catch (IOException e) {
      throw new JXMLException(e.getCause());
    }
  }

  /**
   * This is the method that five the possibility to deserialize the XML content as String in a Java
   * Object. This method required that the object inside the XML is a single object and not an
   * collection.
   *
   * @param value The XML content as String
   * @param clazz The class type where the content need to be bind
   * @param <T> The return type that is the same of the class type passes as parameter
   * @return Return the Java object with the value in the XML content
   */
  public <T> T fromXml(String value, Class clazz) {
    return XMLDecoder.deserialize(value, clazz);
  }

  /**
   * This method is another method used to deserialize the XML in a JAVA object, but with this
   * method is use decode the List of Object. This is an work around, basically the deserialization
   * method implement a AST where a Visitor is applied to deserialize the information in a JAVA
   * object, and it is difficult detect without information on the String format if the XML contains
   * a List of not, because the XML has not convention on that.
   *
   * <p>The possible solution is make an Analysis on the AST and produce a Typed AST but the choise
   * at this point it is given to the user.
   *
   * @param value The XML content as String
   * @param clazz The class type where the content need to be bind
   * @param isCollection a flag that give information to the decoder is need to apply the strategy
   *     to create a list.
   * @param <T>T he return type that is the same of the class type passes as parameter
   * @return The list of Java object of type T decoded from the XML content.
   */
  public <T> List<T> fromXml(String value, Class clazz, boolean isCollection) {
    return XMLDecoder.deserialize(value, clazz, isCollection);
  }
}
