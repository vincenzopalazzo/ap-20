# Advanced Programming 2020-2021 Assignments Solution

## Table of Content

- Introduction
- Assignment One
- Assignment Two

## Introduction

All the solution of the assignment are under the directory with the name assignment_X, and each directory contains the subdirectory
with the solution of one part of the assignment where there is the solution and when possible two additional directories, one with 
with the name docs that contain the documentation auto-generated from the compiler and on called dist that contains the jars to run
the application.

The jar is compiled with the JDK 11 and if the assignment requires developing a UI the jar can be run with the command java -jar name.jar, otherwise
a small command line is implemented and it is possible to explore the options with the following command java -jar name.jar --help.

The solution of the other languages is a developer with a Make file and it supports the make command to run or compiler the library and when it is required
it is supported to make tests to run the tests.

## Assignment One

#### Covid Buss

The jar is available under the "buss_control/dist" directory and the source code is commented to explain the decision taken during the developing phases.

To run the App the command java -jar app-all.jar is supported.

### XML Encoder

The XML encoder is provided as a library without the main because a complete test suite is provided with the Gradle env, so to run the test is possible to run the command
`./gradlew test -i`. after the execution, the XML file is generated at the root of the project.

## Assignment Two

### Haskell: Multiset

A file make is provided, with the following targets

- make: To compiler the library
- make test: To compiler the library and run the tests.

### Java: Map Reduce

The map-reduce is provided as a library without a main and a complete test suite is provided with the Gradle env.

To run the test is possible to run the following command `./gradlew test -i`, the result of the decoding as the file is stored inside the build dir to make the test reproducible.

#### Counting Words

The Counting words are provided as command-line applications, and the app is available under the `ex4/dist` directory.

To run the app is possible to run the following command `java -jar app-all.jar --help`, and an example of the command is the following one `java -jar app-all.jar -i /path/file.txt -o /path/result.cvs`

#### Inverted Index

The Inverted Index is provided as a command-line application and the app is available under the `ex5/dist` directory.

To run the app is possible to run the following command `java -jar app-all.jar --help`, and an example of the command is the following one `java -jar app-all.jar -i /path/files -o /path/result.cvs`

### Python: benchmarks

The benchmark file is provided as a python module and it is used in a module in the next exercise. 

#### CPython multithreading measurements

A make file is provided with the following targets

- make: To run the benchmarks and the results are printed on console and in a CSV file, as the assignment description.


### Documentation

Each assignment contains documentation in HTML format to display the documentation on a web version. The content is under the docs.