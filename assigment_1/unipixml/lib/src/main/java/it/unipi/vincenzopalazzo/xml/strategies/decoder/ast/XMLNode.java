/*
 * Advanced Programming project 2020/2021
 * <p>
 * Copyright (C) 2020-2021 Vincenzo Palazzo vincenzopalazzodev@gmail.com
 * and all professors of the Advanced Programming class
 * http://pages.di.unipi.it/corradini/Didattica/AP-20/
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package it.unipi.vincenzopalazzo.xml.strategies.decoder.ast;

import it.unipi.vincenzopalazzo.xml.strategies.decoder.ast.visitor.IVisitor;
import java.util.ArrayList;
import java.util.List;

/**
 * XML node that represent the intermedia level of the AST tree Is is an implementation of Composite
 * INode
 *
 * @author Vincenzo Palazzo v.palazzo1@studenti.unipi.it
 */
public class XMLNode implements INode {

  private String name;
  private List<INode> children;

  public XMLNode() {
    this.children = new ArrayList<>();
  }

  public XMLNode(String name) {
    this.name = name;
    this.children = new ArrayList<>();
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public List<INode> getChildren() {
    return children;
  }

  public void addChild(INode node) {
    this.children.add(node);
  }

  @Override
  public String toString() {
    return "XMLNode{" + "name='" + name + '\'' + ", children_size=" + children.size() + '}';
  }

  @Override
  public Object accept(IVisitor visitor) {
    return visitor.visitXMLNode(this);
  }
}
