module Ex6 where

import Data.Char (toUpper)


toCapital :: String -> String

toCapital [] = []
toCapital (x:xs) = toUpper (x) : cycle xs where
  cycle [] = []
  cycle (x:xs) = x : cycle xs

--titleCaseR :: String -> String

titleCaseR string = unwords(_titleCaseR (words string)) where
  _titleCaseR [] = []
  _titleCaseR (x:xs) = toCapital x : _titleCaseR xs

--titleCaseC :: String -> String
titleCaseC string =
  unwords(map toCapital (words string ))
