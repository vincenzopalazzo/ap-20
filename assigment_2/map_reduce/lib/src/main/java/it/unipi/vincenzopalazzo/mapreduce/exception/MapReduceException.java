/**
 * Advanced Programming project 2020/2021
 * <p>
 * Copyright (C) 2020-2021 Vincenzo Palazzo vincenzopalazzodev@gmail.com
 * and all professors of the Advanced Programming class
 * http://pages.di.unipi.it/corradini/Didattica/AP-20/
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package it.unipi.vincenzopalazzo.mapreduce.exception;

/**
 * Runtime exception that help to compact the interface of the Framework
 * all the user need to know that the framework throes an exception, and
 * also this help to maintains the interface compact.
 *
 * @author Vincenzo Palazzo v.palazzo1@studenti.unipi.it
 */
public class MapReduceException extends RuntimeException {
    public MapReduceException() {
        super();
    }

    public MapReduceException(String message) {
        super(message);
    }

    public MapReduceException(String message, Throwable cause) {
        super(message, cause);
    }

    public MapReduceException(Throwable cause) {
        super(cause);
    }

    protected MapReduceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
