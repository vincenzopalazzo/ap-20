/*
 * Advanced Programming project 2020/2021
 * <p>
 * Copyright (C) 2020-2021 Vincenzo Palazzo vincenzopalazzodev@gmail.com
 * and all professors of the Advanced Programming class
 * http://pages.di.unipi.it/corradini/Didattica/AP-20/
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package it.unipi.vincenzopalazzo.xml.strategies.decoder.ast.visitor;

import it.unipi.vincenzopalazzo.xml.JXMLException;
import it.unipi.vincenzopalazzo.xml.annotations.XMLabel;
import it.unipi.vincenzopalazzo.xml.annotations.XMLfield;
import it.unipi.vincenzopalazzo.xml.strategies.decoder.ast.XMLLeaf;
import it.unipi.vincenzopalazzo.xml.strategies.decoder.ast.XMLNode;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

/**
 * The implementation of the visitor that implement the decode strategy
 *
 * @author Vincenzo Palazzo v.palazzo1@studenti.unipi.it
 */
public class DecoderVisitor implements IVisitor<Void> {

  private Object target;
  private Class typeCass;

  public DecoderVisitor(Class decoderClass) {
    this.typeCass = decoderClass;
    this.target = createInstance(decoderClass);
  }

  private Object createInstance(Class decoderClass) {
    try {
      Class clazz = Class.forName(decoderClass.getCanonicalName());
      Constructor<?> constructorb = clazz.getConstructors()[0];
      return constructorb.newInstance();
    } catch (InstantiationException
        | IllegalAccessException
        | InvocationTargetException
        | ClassNotFoundException e) {
      throw new JXMLException(e.getCause());
    }
  }

  public Object getTarget() {
    Object returnVal = target;
    target = null;
    return returnVal;
  }

  @Override
  public Void visitXMLNode(XMLNode tag) {
    if (target == null) {
      this.target = createInstance(this.typeCass);
    }
    if (!this.target.getClass().isAnnotationPresent(XMLabel.class))
      return null; // Ignore the object
    tag.getChildren().forEach(node -> node.accept(this));
    return null;
  }

  @Override
  public Void visitorXMLLeaf(XMLLeaf infoTag) {
    Object value = getValueWithType(infoTag);
    try {
      Field field = findFieldWithName(infoTag.getName(), target.getClass());
      if (field != null) {
        setValueToField(field, value);
        return null;
      }
      // Search in the superclass
      this.searchInSuperclass(this.target.getClass(), infoTag.getName(), value);
      return null;
    } catch (IllegalAccessException e) {
      throw new JXMLException(e.getCause());
    }
  }

  private Field findFieldWithName(String name, Class clazz) {
    try {
      return clazz.getDeclaredField(name);
    } catch (NoSuchFieldException e) {
      return null; // Not found
    }
  }

  private void searchInSuperclass(Class from, String nameFiled, Object value)
      throws IllegalAccessException {
    Class className = from;
    // TODO there is a better way?
    while (className != null) {
      Field field = findFieldWithName(nameFiled, className);
      if (field != null) {
        this.setValueToField(field, value);
      }
      className = className.getSuperclass();
    }
  }

  private void setValueToField(Field field, Object value) throws IllegalAccessException {
    if (field.isAnnotationPresent(XMLfield.class)) {
      field.setAccessible(true);
      field.set(target, value);
      field.setAccessible(false); // all the field will be private with this call :(
    }
  }

  private Object getValueWithType(XMLLeaf infoTag) {
    if (String.class.equals(infoTag.getType())) {
      return infoTag.getValue();
    } else if (Integer.class.equals(infoTag.getType())) {
      return Integer.parseInt(infoTag.getValue());
    } else if (Float.class.equals(infoTag.getType())) {
      return Float.parseFloat(infoTag.getValue());
    }
    throw new JXMLException("Type " + infoTag.getType() + "not recognized");
  }
}
