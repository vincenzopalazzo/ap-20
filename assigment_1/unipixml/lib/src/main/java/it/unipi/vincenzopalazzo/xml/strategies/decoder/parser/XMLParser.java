/*
 * Advanced Programming project 2020/2021
 * <p>
 * Copyright (C) 2020-2021 Vincenzo Palazzo vincenzopalazzodev@gmail.com
 * and all professors of the Advanced Programming class
 * http://pages.di.unipi.it/corradini/Didattica/AP-20/
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package it.unipi.vincenzopalazzo.xml.strategies.decoder.parser;

import it.unipi.vincenzopalazzo.xml.JXMLException;
import it.unipi.vincenzopalazzo.xml.strategies.decoder.ast.INode;
import it.unipi.vincenzopalazzo.xml.strategies.decoder.ast.XMLLeaf;
import it.unipi.vincenzopalazzo.xml.strategies.decoder.ast.XMLNode;
import it.unipi.vincenzopalazzo.xml.strategies.decoder.scanner.Token;
import it.unipi.vincenzopalazzo.xml.strategies.decoder.scanner.TokenType;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * XML parser implementation that contains the logic to build the XML DOM from a List of TOKENs
 * received from the XML scanner
 *
 * @author Vincenzo Palazzo v.palazzo1@studenti.unipi.it
 */
public class XMLParser {

  private static final Logger LOGGER = LoggerFactory.getLogger(XMLParser.class);

  private List<Token> tokens;
  private int current;

  public XMLParser(List<Token> tokens) {
    this.tokens = tokens;
    this.current = 0;
  }

  /**
   * Main method to run the parsing of the String of Toeknd
   *
   * @return Return the root of the AST.
   */
  public INode parser() {
    LOGGER.debug("Start Parsing tree");
    INode node = tree();
    LOGGER.debug("End Parsing tree");
    return node;
  }

  /**
   * The core method that contains the logic to parse the String of content and build the AST.
   *
   * @return return the root of the AST
   */
  private INode tree() {
    LOGGER.debug("Reading tree");
    XMLNode root = new XMLNode();
    if (advance().getType() == TokenType.OPEN_TAG) {
      Token nameTag = advance();
      assert (nameTag.getType() == TokenType.NAME_TAG);
      LOGGER.debug("Name tag: " + nameTag.getValue());
      root.setName(nameTag.getValue());
      assert (advance().getType() == TokenType.END_TAG);
      LOGGER.debug("Ending starting tag");
    } else {
      throw new JXMLException("XML bad formed");
    }
    while (!isFinished()) {
      LOGGER.debug("Start analysis on " + peek());
      parseTree(root);
    }
    return root;
  }

  /**
   * Parse the XML intermedia node of the XML, this node contains the class of list definition.
   *
   * @param root Take the actual node that is the "root" of the sub-tree that the parser are
   *     analyzing
   */
  private void parseTree(INode root) {
    if (match(TokenType.OPEN_TAG)) {
      if (peek().getType() == TokenType.NAME_TAG) {
        String name = (String) advance().getValue();
        if (match(TokenType.END_TAG)) {
          INode node = new XMLNode(name);
          XMLNode rootNode = (XMLNode) root;
          rootNode.addChild(node);
          parseTree(node);
          return;
        }
      }
    }
    if (match(TokenType.TYPE_ELEM)) {
      INode child = this.parseXMLLeaf();
      XMLNode rootNode = (XMLNode) root;
      rootNode.addChild(child);
      parseTree(root);
      return;
    }
    if (match(TokenType.CLOSE_TAG) && match(TokenType.NAME_TAG) && match(TokenType.END_TAG)) return;
    throw new JXMLException("Token: " + peek().getType() + " not recognize");
  }

  /**
   * Parse the XML leaf, where the Type and the value of the type is defined
   *
   * @return Return the XML leaf as node of the AST.
   */
  private INode parseXMLLeaf() {
    consume(TokenType.EQ, "Error expected token = but found " + peek().getType());
    Token valueType = advance();
    Class type = toNativeType(valueType);
    Token tmpValue = advance();
    LOGGER.debug("Value expected is the tag value and we have " + tmpValue.getType());
    tmpValue = advance();
    LOGGER.debug(
        "Expected the tag content " + tmpValue.getType() + " with value " + tmpValue.getValue());
    String value = tmpValue.getValue();
    if (match(TokenType.CLOSE_TAG)) {
      if (makeCheck(TokenType.NAME_TAG)) {
        Token token = advance();
        String nameTag = token.getValue();
        if (match(TokenType.END_TAG)) {
          return new XMLLeaf(nameTag, type, value);
        }
      }
    }
    throw new JXMLException("Node leaf with bad format at line " + peek().getLine());
  }

  /**
   * Convert the TokenType that contains the type value into a Java Type
   *
   * @param tmpToken The token type that represents the type value
   * @return return the Java Type wrapper
   */
  private Class toNativeType(Token tmpToken) {
    String value = tmpToken.getValue();
    value = value.replace("\"", "");
    if ("String".equals(value)) {
      return String.class;
    } else if ("int".equals(value)) {
      return Integer.class;
    } else if ("float".equals(value)) {
      return Float.class;
    }
    throw new JXMLException("Parsing error: Native type " + tmpToken.getValue() + " not supported");
  }

  /**
   * Check if the Token as parameter match the token in the actual position in the stream
   *
   * @param args The list of Token to match
   * @return Return true if one of the parameter match with the actual token in the stream
   */
  private boolean match(TokenType... args) {
    for (TokenType arg : args) {
      if (makeCheck(arg)) {
        advance();
        return true;
      }
    }
    return false;
  }

  /**
   * Check if the Token as parameter is equal to the token in actual position of the stream
   *
   * @param type Token that the parser is searching for
   * @return return true if the token as parameter match the content of the stream in the current
   *     position
   */
  private boolean makeCheck(TokenType type) {
    if (isFinished()) return false;
    return peek().getType() == type;
  }

  /**
   * Check if the parser reach the end
   *
   * @return return true if the stream of token is ended.
   */
  private boolean isFinished() {
    return peek().getType() == TokenType.EOF;
  }

  /**
   * Consume the token if it is match with the element in current position of the stream
   *
   * @param type The Token Type to searching for
   * @param errorMessage The error message if the match is not satisfied
   * @return Return the previous token after advance in the stream
   */
  private Token consume(TokenType type, String errorMessage) {
    if (makeCheck(type)) return advance();
    throw new JXMLException(errorMessage);
  }

  /**
   * Advanced in the stream
   *
   * @return Return the previous Token of the stream after advance
   */
  private Token advance() {
    if (!isFinished()) current++;
    return previous();
  }

  /**
   * Return the previous token of the stream
   *
   * @return Return the token in the current position - 1
   */
  private Token previous() {
    return this.tokens.get(current - 1);
  }

  /**
   * Take the Token in the current position
   *
   * @return Return the token in the current position.
   */
  private Token peek() {
    return this.tokens.get(current);
  }
}
