plugins {
    `java-library`
}

repositories {
    jcenter()
}

dependencies {
    implementation(files("devlib/CovidController.jar"))

    implementation("ch.qos.logback:logback-classic:1.2.3")
    implementation("ch.qos.logback:logback-core:1.2.3")
    implementation("org.slf4j:slf4j-api:1.7.25")

    testImplementation("junit:junit:4.13")
}
