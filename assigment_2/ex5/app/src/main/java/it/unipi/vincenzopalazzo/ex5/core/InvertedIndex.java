/**
 * Advanced Programming project 2020/2021
 * <p>
 * Copyright (C) 2020-2021 Vincenzo Palazzo vincenzopalazzodev@gmail.com
 * and all professors of the Advanced Programming class
 * http://pages.di.unipi.it/corradini/Didattica/AP-20/
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package it.unipi.vincenzopalazzo.ex5.core;

import it.unipi.vincenzopalazzo.ex5.exception.InvertedIndexException;
import it.unipi.vincenzopalazzo.ex5.utils.WriterProxy;
import it.unipi.vincenzopalazzo.mapreduce.AbstractMapReduce;
import it.unipi.vincenzopalazzo.mapreduce.utils.Pair;
import it.unipi.vincenzopalazzo.mapreduce.utils.Reader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

/**
 * InvertedIndex is a implementation of MapReduce that take in input an path of inputs file
 * tokenize it in words and perform the map reduce logic on it.
 * At the end of the execution it will save the result of a cvs file.
 *
 * @author Vincenzo Palazzo on  v.palazzo1@studenti.unipi.it
 */
public class InvertedIndex extends AbstractMapReduce<String, List<String>,
        String, Pair<String, Integer>, Pair<String, Integer>> {

    private static final Logger LOGGER = LoggerFactory.getLogger(InvertedIndex.class);

    private String absoluteInputPath;
    private String absoluteOutputPath;

    /**
     * Create a instance of the class without specify the
     * input path and output path.
     *
     * Usually when the user have not available the path but want to create the
     * instance of the class and use the setter method to setup the paths.
     *
     * An example of the usage is inside the unit testing of the map reduce project
     */
    public InvertedIndex() { }

    /**
     * Create a instance of the class with the absolute path specified
     * in the constructor
     * @param inputPath Input path of the file where perform the logic
     * @param outputPath Output path where same the result in cvs form
     */
    public InvertedIndex(String inputPath, String outputPath) {
        this.absoluteInputPath = inputPath;
        this.absoluteOutputPath = outputPath;
    }

    public void setAbsoluteInputPath(String absoluteInputPath) {
        this.absoluteInputPath = absoluteInputPath;
    }

    public void setAbsoluteOutputPath(String absoluteOutputPath) {
        this.absoluteOutputPath = absoluteOutputPath;
    }

    /**
     * This method perform the read operation, usually it takes a data from
     * any type of repository and convert these data in a Stream of Pair.
     * @return A stream of pairs of type K1, V1
     */
    @Override
    protected Stream<Pair<String, List<String>>> read() throws InvertedIndexException {
        if (absoluteInputPath == null) {
            throw new InvertedIndexException("Input path is undefined");
        }
        Path path = Path.of(absoluteInputPath);
        Reader reader = new Reader(path);
        try {
            return reader.read();
        } catch (IOException e) {
            // Wrapping th exception in a framework interface.
            // When the user receive the stack trace he can understand that the problem is
            // cause ny the framework.
            throw new InvertedIndexException(e.getLocalizedMessage(), e.getCause());
        }

    }

    /**
     * The function map is an implementation to of inverted index to tokenize each line by space and alphanumeric char
     * In addition, it make the number of the line with an atomic integer, this is not exactly a good
     * thing to do, it is better that the reader return the stream with the index of the line.
     * This give the possibility to run the code in parallel without care about the order of the line.
     * In this case the atomicInteger is used only to have consistency between value in case of parallel execution
     * but it not avoid the order of the line number, a line in position X can be set as in position X + (or -) Y because
     * the parallel execution don't have enough information to know that the order of the line care.
     *
     * TODO: Fix the bug of indexing line.
     *
     * The core of this function logic is filter the line, and tokenize in words and each words contains a pair where
     * the reference of the file and the position in this file of the word.
     *
     * @param stream of pairs of type (K1, V1)
     * @return stream of pairs of type (K2, V2)
     */
    @Override
    protected Stream<Pair<String, Pair<String, Integer>>> map(Stream<Pair<String, List<String>>> stream) {
        return stream.flatMap(pair -> {
            var atomicIndex = new AtomicInteger(0);
            return pair.getValue()
                    .stream()
                    .sequential()
                    .peek(LOGGER::debug)
                    // line, nameFile, index
                    .map(line -> new Pair<>(line, new Pair<>(pair.getKey(), atomicIndex.incrementAndGet())));
            // Stream of <Line, <FileName, Pos>>
        }).flatMap(newPair -> Arrays.stream(newPair.getKey()
                .toLowerCase()
                .trim()
                .replaceAll("[^a-zA-Z0-9]+", " ")
                .split(" "))
                .filter(word -> word.length() > 3)
                .peek(LOGGER::debug)
                // Create a list of Pair<word, Pair<File, Pos>>
                .map(word -> new Pair<>(word, newPair.getValue()))
        );
    }

    /**
     * The reduce function is an implementation to inverted index in a file, this function is execute after the
     * join operation that perform the aggregate operation and return a stream of pairs (Word, List of Pair (FileName, Pos)),
     * this function take list of pairs and return a stream where contains only pais grouped by position index
     *
     * @param stream of pairs (K2, List of V2)
     * @return stream of pairs (K2, R)
     */
    @Override
    protected Stream<Pair<String, Pair<String, Integer>>> reduce(Stream<Pair<String, List<Pair<String, Integer>>>> stream) {
        return stream.flatMap(index -> index.getValue().stream().map(pair -> new Pair<>(index.getKey(), pair)));
    }

    /**
     * The write function takes the reduce  result and store this function in a csv file where
     * the user specify the path with the name included.
     *
     * @param stream is a stream of pairs of type (K2, R)
     */
    @Override
    protected void write(Stream<Pair<String, Pair<String, Integer>>> stream) {
        if (absoluteOutputPath == null || absoluteOutputPath.isEmpty())
            if (this.absoluteInputPath.contains(".txt"))
                this.absoluteOutputPath = this.absoluteInputPath + ".csv";
            else if (this.absoluteInputPath.endsWith("/"))
                this.absoluteOutputPath = this.absoluteInputPath + "out.csv";
            else
                this.absoluteOutputPath = this.absoluteInputPath + "/out.csv";
        File file = new File(this.absoluteOutputPath);
        try {
            WriterProxy.writeInvertedIndex(file, stream);
        } catch (FileNotFoundException e) {
            throw new InvertedIndexException(e.getLocalizedMessage(), e.getCause());
        }
    }
}
