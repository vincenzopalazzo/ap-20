module Ex9 where

data IntTree = Leaf Int | Node (Int, IntTree, IntTree)

--tmap :: func -> tree -> tree
tmap func (Leaf val) = Leaf (func val)
tmap func (Node(val, left, right)) =
  Node ((func val), tmap func left, tmap func right)

succTree tree =
  tmap succ tree

-- sumSucc :: (IntTree tree) => tree -> Int
sumSucc tree = _sumSucc (succTree tree) where
  _sumSucc (Leaf (val)) = val
  _sumSucc (Node (val, leaft, right)) =
    val + (_sumSucc leaft) + (_sumSucc right)
