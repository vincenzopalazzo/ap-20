/*
 * Advanced Programming project 2020/2021
 * <p>
 * Copyright (C) 2020-2021 Vincenzo Palazzo vincenzopalazzodev@gmail.com
 * and all professors of the Advanced Programming class
 * http://pages.di.unipi.it/corradini/Didattica/AP-20/
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

package it.unipi.vincenzopalazzo.mapreduce;

import it.unipi.vincenzopalazzo.mapreduce.utils.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Stream;

/**
 * @param <K1> need to be comparable to give the possibility to the framework to work well with the comparison
 * @param <V1> The type of the value to be read as stream.
 * @param <K2> need to be comparable to give the possibility to the framework to work well with the comparison
 * @param <V2> The type of the value as result of the first map method application.
 * @param <R> The type of result, in the example where this framework is used this can be omitter because the result
 *            type is equal to the V2 type. However it is possible have some different type result in some different application.
 *
 * @author Vincenzo Palazzo v.palazzo1@studenti.unipi.it
 */
public abstract class AbstractMapReduce<K1 extends Comparable, V1, K2 extends Comparable, V2, R> {

    /**
     * This method perform the read operation, usually it takes a data from
     * any type of repository and convert these data in a Stream of Pair.
     * @return A stream of pairs of type K1, V1
     */
    protected abstract Stream<Pair<K1, V1>> read();

    /**
     * The function map is a primitive operation that the user have to implement.
     * @param stream of pairs of type (K1, V1)
     * @return stream of pairs of type (K2, V2)
     */
    protected abstract Stream<Pair<K2, V2>> map(Stream<Pair<K1, V1>> stream);

    /**
     *
     * The reduce function is a primitive operation that the user have to implement.
     * @param stream of pairs (K2, List of V2)
     * @return stream of pairs (K2, R)
     */
    protected abstract Stream<Pair<K2, R>> reduce(Stream<Pair<K2, List<V2>>> stream);

    /**
     * the write operation is a primitive operation that the user need to implement.
     * @param result is a stream of pairs of type (K2, R)
     */
    protected abstract void write(Stream<Pair<K2, R>> result);

    /**
     * This function is a hook operation that help to establish the equality between object of value K2,
     * in according with the description of Dataflow of https://en.wikipedia.org/wiki/MapReduce#Dataflow
     * @param first the first value to compare of type K2
     * @param second the second value to compare of type K2
     * @return an integer with the same semantics of equals in the comparable java interface
     */
    protected int compare(K2 first, K2 second) {
        return first.compareTo(second);
    }

    /**
     * This function is a concrete operation that make of the Template patter, this include the
     * fixed logic of the algorithm that the user need to respect because it can not modify it.
     * @param stream of pairs <K2, V2>
     * @return stream of pairs <K2, List<V2>>
     */
    private Stream<Pair<K2, List<V2>>> join(Stream<Pair<K2, V2>> stream) {
        Map<K2, List<V2>> sorting = new TreeMap<>(this::compare);

        stream.forEach(elem -> {
            if (!sorting.containsKey(elem.getKey()))
                sorting.put(elem.getKey(), new ArrayList<>());
            var value = sorting.get(elem.getKey());
            value.add(elem.getValue());
        });
        Stream<Pair<K2, List<V2>>> result = sorting.entrySet()
                .stream()
                .map(elem -> new Pair<>(elem.getKey(), elem.getValue()));
        return result;
    }

    /**
     * This function is a concrete operation that implement the the final user
     * need to call to run the framework.
     * The user can not redefine it because it is defined as final and this mean that
     * no dynamic dispaccing is applied.
     */
    public final void start() {
        Stream<Pair<K1, V1>> input_stream = read();
        Stream<Pair<K2, V2>> mapped_stream = map(input_stream);
        Stream<Pair<K2, List<V2>>> grouped_stream = join(mapped_stream);
        Stream<Pair<K2, R>> reduced_stream = reduce(grouped_stream);
        write(reduced_stream);
    }
}
