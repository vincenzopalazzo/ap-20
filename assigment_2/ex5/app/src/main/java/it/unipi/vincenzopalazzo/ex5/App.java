/**
 * Advanced Programming project 2020/2021
 * <p>
 * Copyright (C) 2020-2021 Vincenzo Palazzo vincenzopalazzodev@gmail.com
 * and all professors of the Advanced Programming class
 * http://pages.di.unipi.it/corradini/Didattica/AP-20/
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package it.unipi.vincenzopalazzo.ex5;

import it.unipi.vincenzopalazzo.ex5.core.InvertedIndex;
import picocli.CommandLine;

import java.util.concurrent.Callable;

@CommandLine.Command(name = "inverted-index", mixinStandardHelpOptions = true, version = "inverted-index 0.1")
public class App implements Callable<Integer> {

    @CommandLine.Option(names = {"-i", "--input"}, description = "Absolute path of the input file", required = true)
    private String inputPath;
    @CommandLine.Option(names = {"-o", "--output"}, description = "Absolut path of the output file")
    private String outputPath;

    private InvertedIndex countingWords;

    public App() {
        this.countingWords = new InvertedIndex();
    }

    @Override
    public Integer call() {
        this.countingWords.setAbsoluteInputPath(this.inputPath);
        this.countingWords.setAbsoluteOutputPath(this.outputPath);
        this.countingWords.start();
        return 0;
    }

    public static void main(String[] args) {
        int existCode = new CommandLine(new App()).execute(args);
        System.exit(existCode);
    }
}

