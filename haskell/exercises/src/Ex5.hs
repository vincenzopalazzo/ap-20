module Ex5 where

filterOddR :: (Show a) => [a] -> [a]

filterOddR [] = []
filterOddR list = _filterOddR list 0 where
  _filterOddR [] _ = []
  _filterOddR (x:xs) index =
        if (odd index) then []  ++ (_filterOddR xs (index + 1))
        else [x] ++ (_filterOddR xs (index + 1))

filterOddC :: (Show a) => [a] -> [a]

filterOddC list =
  map (fst $) (filter (\x -> (even (snd x))) (zip list [0..(length list)]))
