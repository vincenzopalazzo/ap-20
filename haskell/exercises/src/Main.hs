import Ex1
import Ex2
import Ex3
import Ex4
import Ex5
import Ex6
import Ex7
import Ex8
import Utils
import GHC.List

listR = myReplicateR 12 "aba"
listC = myReplicateC 12 "aba"

sumR = sumOddR [1..30]
sumC = sumOddC [1..30]

replicateR = replR listR 12
replicateC = replC listC 12

sumAStartR = totalLengthR ["Alibaba", "dida", "alibaba"]
sumAStartC = totalLengthC ["Alibaba", "dida", "alibaba"]

filterOddPosR = filterOddR [1, 2, 3, 4, 5, 6]
filterOddPosC = filterOddC [1, 2, 3, 4, 5, 6]

titleCaseStringR = titleCaseR "haskell by examples"
titleCaseStringC = titleCaseC "haskell by examples"

sumPalindromicR = countVowelPaliR ["anna", "banana", "civic", "mouse"]
sumPalindromicC = countVowelPaliC ["anna", "banana", "civic", "mouse"]

myMapRes = map' (1 +) [1, 2, 3, 4, 5]
mapRes = map (1 +) [1, 2, 3, 4, 5]
mapResPersonal = myMap (1 +) [1, 2, 3, 4, 5]
secondVRes = secondVersion (1 +) [1, 2, 3, 4, 5]

main =
  putStrLn (
  (
    "-------------- Ex 1 --------------\n" ++
    (cp_log listR) ++ "\nSize list is " ++ (show (Prelude.length listR)))
    ++ "\n" ++
    ((cp_log listC) ++ "\nSize list is " ++ (show (Prelude.length listC)))
    ++ "\n--------- Ex 2 -------------\n" ++
    "Sum odd recursive is: " ++ (show sumR)
    ++ "\nSum odd combinators " ++ (show sumC)
    ++ "\n------------ Ex 3 ---------------\n" ++
    ((cp_log replicateR) ++ "\nSize list is " ++ (show (Prelude.length replicateR)))
    ++ "\n" ++
    ((cp_log replicateC) ++ "\nSize list is " ++ (show (Prelude.length replicateC)))
    ++ "\n----------- Ex 4 ----------------\n" ++
    "Sum of the list with A R: " ++ (show sumAStartR)
    ++ "\nSum of the list with A C: " ++ (show sumAStartC)
    ++ "\n----------- Ex 5 ----------------\n"
    ++ "Filter odd position rec: " ++ (cp_log filterOddPosR)
    ++ "\nFilter odd position com: " ++ (cp_log filterOddPosC)
    ++ "\n----------- Ex 6 ----------------\n"
    ++ "Title case String recursive: " ++ titleCaseStringR
    ++ "\n Title case String recursive: " ++ titleCaseStringC
    ++ "\n------------- Ex 7 --------------\n"
    ++ "Count palindromic strings rec: " ++ (show sumPalindromicR)
    ++ "\nCount palindromic string c: " ++ (show sumPalindromicC)
    ++ "\n------------- Ex 8 --------------\n"
    ++ "My map result: " ++ (cp_log myMapRes)
    ++ "\nMap result:    " ++ (cp_log mapRes)
    ++ "\nMap personal:  " ++ (cp_log mapResPersonal)
    ++ "\nMap second version: " ++ (cp_log secondVRes)
  )
