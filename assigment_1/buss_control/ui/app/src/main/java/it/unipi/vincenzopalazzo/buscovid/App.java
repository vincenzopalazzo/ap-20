/**
 * Advanced Programming project 2020/2021
 *
 * <p>Copyright (C) 2020-2021 Vincenzo Palazzo vincenzopalazzodev@gmail.com and all professors of
 * the Advanced Programming class http://pages.di.unipi.it/corradini/Didattica/AP-20/
 *
 * <p>This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * <p>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * <p>You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
package it.unipi.vincenzopalazzo.buscovid;

import it.unipi.vincenzopalazzo.buscovid.ui.BusBoard;
import javax.swing.SwingUtilities;

/** @author Vincenzo Palazzo <v.palazzo1@studenti.unipi.it> */
public class App {

  public static void main(String[] args) {
    SwingUtilities.invokeLater(() -> new BusBoard().initView());
  }
}
