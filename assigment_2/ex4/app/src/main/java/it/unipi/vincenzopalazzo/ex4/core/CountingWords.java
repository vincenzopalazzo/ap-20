/**
 * Advanced Programming project 2020/2021
 * <p>
 * Copyright (C) 2020-2021 Vincenzo Palazzo vincenzopalazzodev@gmail.com
 * and all professors of the Advanced Programming class
 * http://pages.di.unipi.it/corradini/Didattica/AP-20/
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package it.unipi.vincenzopalazzo.ex4.core;

import it.unipi.vincenzopalazzo.mapreduce.AbstractMapReduce;
import it.unipi.vincenzopalazzo.mapreduce.exception.PicoMapReduceException;
import it.unipi.vincenzopalazzo.mapreduce.utils.Pair;
import it.unipi.vincenzopalazzo.mapreduce.utils.Reader;
import it.unipi.vincenzopalazzo.mapreduce.utils.Writer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * CountingWords is a implementation of MapReduce that take in input an path of inputs file
 * tokenize it in words and perform the map reduce logic on it.
 * At the end of the execution it will save the result of a cvs file.
 *
 * @author Vincenzo Palazzo <v.palazzo1@studenti.unipi.it>
 */
public class CountingWords extends AbstractMapReduce<String, List<String>, String, Integer, Integer> {

    private String absoluteInputPath;
    private String absoluteOutputPath;

    /**
     * Create a instance of the class without specify the
     * input path and output path.
     *
     * Usually when the user have not available the path but want to create the
     * instance of the class and use the setter method to setup the paths.
     *
     * An example of the usage is inside the unit testing of the map reduce project
     */
    public CountingWords() { }

    /**
     * Create a instance of the class with the absolute path specified
     * in the constructor
     * @param absoluteInputPath Input path of the file where perform the logic
     * @param absoluteOutputPath Output path where same the result in cvs form
     */
    public CountingWords(String absoluteInputPath, String absoluteOutputPath) {
        this.absoluteInputPath = absoluteInputPath;
        this.absoluteOutputPath = absoluteOutputPath;
    }

    public String getAbsoluteInputPath() {
        return absoluteInputPath;
    }

    public void setAbsoluteInputPath(String absoluteInputPath) {
        this.absoluteInputPath = absoluteInputPath;
    }

    public String getAbsoluteOutputPath() {
        return absoluteOutputPath;
    }

    public void setAbsoluteOutputPath(String absoluteOutputPath) {
        this.absoluteOutputPath = absoluteOutputPath;
    }

    /**
     * This method perform the read operation, usually it takes a data from
     * any type of repository and convert these data in a Stream of Pair.
     * @return A stream of pairs of type K1, V1
     */
    @Override
    protected Stream<Pair<String, List<String>>> read() {
        if (absoluteInputPath == null) {
            throw new PicoMapReduceException("Input path is undefined");
        }
        Path path = Path.of(absoluteInputPath);
        Reader reader = new Reader(path);
        try {
            return reader.read();
        } catch (IOException e) {
            // Wrapping th exception in a framework interface.
            // When the user receive the stack trace he can understand that the problem is
            // cause ny the framework.
            throw new PicoMapReduceException(e.getLocalizedMessage(), e.getCause());
        }
    }

    /**
     * The function map is an implementation to tokenize each line by space and alphanumeric char
     * In addition, it filters the words by length and takes all the words greater or equal to 3 characters.
     * As a result of the function a stream is returned of pairs where each pair contains the (word, counter).
     *
     * @param stream of pairs of type <K1, V1>
     * @return stream of pairs of type <K2, V2>
     */
    @Override
    protected Stream<Pair<String, Integer>> map(Stream<Pair<String, List<String>>> stream) {
        Stream<Pair<String, Integer>> mapResult = stream.parallel().flatMap(value ->
                value.getValue()
                        .stream()
                        //.parallel() // Run it in parallel
                        // remove all not alphanumerical element in the line
                        .map(line -> line.replaceAll("[^a-zA-Z0-9]+", " ").toLowerCase().split(" "))
                        // move all line to lower case and after make a split of line by space
                        .map(Arrays::stream)   // convert the array directly into stream
                        // remove all element that are smaller that 3 elements
                        .flatMap(line -> line.filter(word -> word.length() > 3)
                                .collect(
                                        Collectors.toMap(Function.identity(), word -> 1, Integer::sum)
                                )
                                .entrySet()
                                .stream()
                                .map(entry -> new Pair<>(entry.getKey(), entry.getValue()))

                        )
        );
        return mapResult;
    }

    /**
     * The reduce function is an implementation to count the words in a file, this function is execute after the
     * join operation that perform the aggregate operation and return a stream of pairs <Key, List<Value>>,
     * this function take list of function for each value and reduce this element with a unique function formed to
     * the sum of all values.
     *
     * @param stream of pairs <K2, List<V2>>
     * @return stream of pairs <K2, R>
     */
    @Override
    protected Stream<Pair<String, Integer>> reduce(Stream<Pair<String, List<Integer>>> stream) {
        Stream<Pair<String, Integer>> reduceResult = stream.parallel().map(value -> {
            int sum = value.getValue().stream().parallel().reduce(1, Integer::sum);
            return new Pair<>(value.getKey(), sum);
        });
        return reduceResult;
    }

    /**
     * The write function takes the reduce  result and store this function in a csv file where
     * the user specify the path with the name included.
     *
     * @param result is a stream of pairs of type <K2, R>
     */
    @Override
    protected void write(Stream<Pair<String, Integer>> result) {
        if (absoluteOutputPath == null || absoluteOutputPath.isEmpty())
            this.absoluteOutputPath = this.absoluteInputPath + ".csv";
        File file = new File(this.absoluteOutputPath);
        try {
            Writer.write(file, result);
        } catch (FileNotFoundException e) {
            throw new PicoMapReduceException(e.getLocalizedMessage(), e.getCause());
        }
    }
}
