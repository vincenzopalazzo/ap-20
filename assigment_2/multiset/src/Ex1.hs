module Ex1 (ListBag(LB), wf, empty, singleton, fromList, toList, isEmpty, mul, sumBag, insert)
where

data ListBag a = LB [(a, Int)]
  deriving (Show, Eq)

{-|
  The method wf implement the logic to check if a multitest (ListBad) is well
  formed.

  This method use the tail recursion to scan the list bag and try to avoid the usage of
  conditional statment in favor of the patter matching to enable more functional style.

  The convention of the variable name are elm = element and ml = multiplicity, and the tail
  function start with the _ in front of the name.
-}
wf :: Eq a => ListBag a -> Bool
wf bag = _wf bag where
  _wf (LB []) = True
  _wf (LB((elm, ml):xs))
    | (ml <= 0 || (mul elm (LB xs)) > 0) = False
    | otherwise = (_wf (LB xs))

-- Multised helper functions

{--
 This is a helper method that aggregate (zip) the list of element compose from [(elem, multiplicity)]
 in one list where each key is aggregate with the multiplicity
 the function use the tail recursion to minimize the space used with big data structure and also
 to mantains the same code convention used in the file.

 In addition the aggregate function name is taken from a StackOverlow post that suggest to use the
 shortcut (==x) and (/=x) instend the of the long form with the anonymus function like \y -> x == y
 to increase readability of the code.
-}
aggregate :: Eq a => [a] -> [(a, Int)]
aggregate list = _aggregate list where
  _aggregate [] = []
  _aggregate (x:xs) = (x, 1 + length (filter (==x) xs) ) : _aggregate (filter (/= x) xs)

-- Multiset constructors

{-|
 The empty function is a constructor of the ListBag that return a empty container.
-}
empty :: ListBag l
empty = LB []

{-|
  The singleton function give the possibility to create a container (ListBag) with only one
  single item inside the container.
-}
singleton :: a -> ListBag a
singleton a = LB [(a, 1)]

{-|
 fromList is a method that give the possibility to create a container from  a list of element
 formed from (element, multiplicity)
-}
fromList :: Eq a => [a] -> ListBag a
fromList withList = LB (aggregate withList)

{-|
 isEmpty give the possibility to check if a container (ListBag is empty)
-}
isEmpty :: ListBag l -> Bool
isEmpty (LB []) = True
isEmpty _       = False

{-|
 mul is a function that give the possibility to have the multiplicity of an element a.

 Under the hood the function use the tail recursion to improve the space usage and the cost of
 the function is O(N) because the algorithm need to fund the element and scan the list bag.
 One possible improvement is to sort the list by element and use the binary search to find the element
 in a sorted list.
-}
mul :: Eq a => a -> ListBag a -> Int
mul elm listBag = _mul elm listBag where
  _mul _ (LB []) = 0
  _mul a (LB ((el, rep):xs) )
    | a == el = rep
    | otherwise = (_mul a (LB xs))

{-|
 toList is a function that give the possibility to return convert the ListBag in a list
 of element where each item has the (elem, multiplicity) form.

 The function under the hood use the tail recursion to improve the space usage. The cost of the
 function can be bad if the number of replica is big or at list = N, in this case the cost of the
 algorithm can be quadratic because each element of the container need to be replicated in the list.
-}
toList :: ListBag a -> [a]
toList listBag = _toList listBag where
  _toList (LB [])            = []
  _toList (LB((el, rep):xs)) = (replicate rep el) ++ (_toList (LB xs))

{-|
 sumBag is a function that give the possibility to merge two container (ListBag)
 in only one container.

 Under the hood this function used the fromList and toList so the complexity of this algorithm depends
 from these function, and in particular from the miltiplicty of each element in the container (ListBag).
 However the space used from this function is optimized because each subfunction use the tail recursion.
-}
sumBag :: Eq a => ListBag a -> ListBag a -> ListBag a
sumBag bagOne bagTwo = fromList((toList bagOne) ++ (toList bagTwo))

{-|
 The insert function insert the element in the container (ListBag).

 under the hood this implementation use the mul to get the mulitplicity of an
 element inside the container, so the complexity of the insert algorithm is
 O(1) because it put on top of the list the pair (elem, mulitplicty).

 This function is used to speed up some custom usage of the component (ListBag)
 in other implementation. Es. mapLB in Ex2.
-}
insert :: Eq a => ListBag a -> (a, Int) -> ListBag a
insert (LB xs) (elm, ml) = LB ([(elm, ml)] ++ xs)
