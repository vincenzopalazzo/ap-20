plugins {
    application
}

repositories {
    jcenter()
}

dependencies {
    implementation("io.github.vincenzopalazzo:material-ui-swing:1.1.2-rc2")
    implementation("io.github.material-ui-swing:SwingSnackBar:0.0.2")
    implementation("io.github.material-ui-swing:DarkStackOverflowTheme:0.0.1-rc3")
    implementation("ch.qos.logback:logback-classic:1.2.3")
    implementation("ch.qos.logback:logback-core:1.2.3")
    implementation("org.slf4j:slf4j-api:1.7.25")
    
    testImplementation("junit:junit:4.13")
}

application {
    mainClass.set("it.unipi.vincenzopalazzo.App")
}
