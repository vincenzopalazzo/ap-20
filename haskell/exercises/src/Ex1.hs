module Ex1 where

myReplicateR 1 x = [x]
myReplicateR a x = [x] ++ (myReplicateR (a - 1) x)


myReplicateC a b = map (\_ -> b) [1..a]
