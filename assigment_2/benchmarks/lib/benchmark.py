"""
Benchmark Module that implement all the logic of a benchmark framework
the method exported is only the benchmark decorator.
"""

import threading
import time
import csv
import os
import functools
from lib.console_writer import ColoredPrint


ui_printer = ColoredPrint()


def benchmark(
    func=None,
    warmups: int = 0,
    iter: int = 1,
    verbose: bool = False,
    csv_file: str = None,
) -> object:
    """
    Function decorator to wrap the method func inside a benchmark logic to make
    some analysis on the execution time of the function

    :param: func is the function where the user want make the benchmarks
    :param: warmups is the number of execution that the function need to complete before start the real benchmark
    :param: iter is the numbers of execution that the function need to complete before return the measurements.
    :param: verbose is a boolean value to enable the version mode during the execution.
    :param: csv_file is the name of the file where the user want to store the result.
    """
    if warmups < 0 or iter < 1:
        raise Exception("Value warmups or iter wrong")

    if func is None:
        return functools.partial(
            benchmark, warmups=warmups, iter=iter, verbose=verbose, csv_file=csv_file
        )

    @functools.wraps(func)
    def wrapper_function(*args, **kwargs):
        warm = run_function_with_result(warmups, func, "WARM", *args, **kwargs)
        invoke = run_function_with_result(iter, func, "NORMAL", *args, **kwargs)

        print_verbose_result("warmups", warm.items(), verbose)
        print_verbose_result("normal", invoke.items(), verbose)
        print_result(invoke)
        store_result_on_csv(warm, csv_file, True)
        store_result_on_csv(invoke, csv_file, False)

    return wrapper_function


def print_result(result: list):
    """
    helper function to print the final result of the benchmaek.

    :param: result is the list of pairs (key, value) that contains the result of execution.
    """
    timer_result = result.values()
    # I used the method sum that perform the reduce logic
    # and return the sum of the items
    sum_timing = sum(timer_result, 0)
    sum_timing /= len(timer_result)
    ui_printer.success(
        "Avrage Time is ", sum_timing, " with ", len(timer_result), "iteration"
    )


def print_verbose_result(prefix: str, result: list, enabled: bool = False):
    """
    Helper function that contains the logic to print the verbose information.

    :param prefix: Is the prefix string to add in front of the verbose result.
    :param result: Is the list of pairs (iter, time) as result of execution.
    :param enabled: boolean flag to enable the verbose mode.
    """
    if enabled is True and len(result) > 0:
        ui_printer.warn("***************   Verbose mode enabled   ******************")
        time_sum = 0.0
        for key, value in result:
            ui_printer.info(prefix, " -> ", f"{key:6}", "end in: ", value)
            time_sum += value
        time_sum = time_sum / len(result)
        ui_printer.success(f"{prefix:6}", "Average time: ", time_sum)


def store_result_on_csv(results: list, name_file: str, warmups: bool = False) -> None:
    """
    Helper function that give the possibility to serialize the con
    on a cvs file.
    :param: results: It is the list of result to store on file.
    :param: name_file: It is the name of the file where store the result.
    :param: warmups a flat to knwo if this result is a result of the warmups.
    """
    if name_file is not None and len(results) > 0:
        with open(name_file, "w") as file_csv:
            writer = csv.writer(file_csv)

            writer.writerow(["Source", threading.current_thread().getName(), ""])
            writer.writerow(["Iteration", "is warmup", "timing"])
            typ = "Yes" if warmups is True else "No"
            for iteration, time in results.items():
                iter_num = iteration.split("_")[1]
                writer.writerow([iter_num, typ, time])
            sum_item = sum(results.values(), 0)
            sum_item /= len(results)
            writer.writerow(["Average", sum_item])
            ui_printer.info("Result stored in the file named", name_file)


def run_function_with_result(
    iteration: int, function: object, name_bm: str, *args, **kwargs
) -> dict:
    """
    A helper function that contains the logic to run the function
    where the user want run the benchmark and store the result for
    each operation in a dictionary

    :param: iteration is the number of the time that this function need to be execute
    :param: function is the function that is passed with in the decorator function
    :param: name_bm is the name of the file where store the information
    :param: *args is the number of the argument in C style to pass at the function
    :param: **kwargs is the array of argument to pass to the function.
    """
    results = dict()
    for i in range(iteration):
        start = time.time()
        function(*args, **kwargs)
        done = time.time()
        key = "BM_{}".format(i)
        results[key] = done - start
    return results
