module Ex8 where

--myMap :: (f -> a -> a) => f -> [a] -> [a]

myMap function list =
  foldl (\x elem -> x ++ [function elem]) [] list

-- Source: https://stackoverflow.com/a/5727490/14933807
map' :: (a -> b) -> [a] -> [b]
map' f xs = foldr (\y ys -> (f y):ys) [] xs

-- Source https://stackoverflow.com/a/3429693/14933807
secondVersion f xs = foldr (\x ys -> f x : ys) [] xs
