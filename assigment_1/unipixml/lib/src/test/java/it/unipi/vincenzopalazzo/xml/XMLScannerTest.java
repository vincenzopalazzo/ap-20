package it.unipi.vincenzopalazzo.xml;

import it.unipi.vincenzopalazzo.xml.strategies.decoder.scanner.Token;
import it.unipi.vincenzopalazzo.xml.strategies.decoder.scanner.TokenType;
import it.unipi.vincenzopalazzo.xml.strategies.decoder.scanner.XMLScanner;
import java.util.List;
import junit.framework.TestCase;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class XMLScannerTest {

  private static final Logger LOGGER = LoggerFactory.getLogger(XMLScannerTest.class);

  @Test
  public void testScanSimpleOpenTag() {
    String portionXml = "<Student> Value </Student>";
    XMLScanner scanner = new XMLScanner(portionXml);
    List<Token> tokens = scanner.scanTokens();
    // The tokens of the xml are
    // 1. open tag = <
    // 2. tag content = Student
    // 3. end tag = >
    // 4 value tag
    // 5 close tag
    // 6 name tag
    // 7 end tag
    // 8. EOF
    TestCase.assertEquals(8, tokens.size());
    TestCase.assertEquals(TokenType.OPEN_TAG, tokens.get(0).getType());
    TestCase.assertEquals(TokenType.NAME_TAG, tokens.get(1).getType());
    TestCase.assertEquals(TokenType.END_TAG, tokens.get(2).getType());
    TestCase.assertEquals(TokenType.VALUE_TAG, tokens.get(3).getType());
    TestCase.assertEquals(TokenType.CLOSE_TAG, tokens.get(4).getType());
    TestCase.assertEquals(TokenType.NAME_TAG, tokens.get(5).getType());
    TestCase.assertEquals(TokenType.END_TAG, tokens.get(6).getType());
    TestCase.assertEquals(TokenType.EOF, tokens.get(7).getType());
  }

  @Test
  public void testScanCompleteObject() {
    String portionXml =
        "<Student>\n"
            + "\t<firstName type=\"String\">Vincenzo</firstName>\n"
            + "\t<age type=\"int\">24</age>\n"
            + "</Student>";
    XMLScanner scanner = new XMLScanner(portionXml);
    List<Token> tokens = scanner.scanTokens();

    TestCase.assertEquals(27, tokens.size());
    TestCase.assertEquals(TokenType.OPEN_TAG, tokens.get(0).getType());
    TestCase.assertEquals(TokenType.NAME_TAG, tokens.get(1).getType());
    TestCase.assertEquals(TokenType.END_TAG, tokens.get(2).getType());
    TestCase.assertEquals(TokenType.OPEN_TAG, tokens.get(3).getType());
    TestCase.assertEquals(TokenType.EOF, tokens.get(tokens.size() - 1).getType());
  }

  @Test
  public void testScanCompleteObjectWithPreamble() {
    String portionXml =
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
            + "<Students>\n"
            + "<Student>\n"
            + "\t<firstName type=\"String\">Vincenzo</firstName>\n"
            + "\t<age type=\"int\">24</age>\n"
            + "</Student>\n"
            + "</Students>";
    XMLScanner scanner = new XMLScanner(portionXml);
    List<Token> tokens = scanner.scanTokens();
    for (Token t : tokens) LOGGER.debug(t.toString());
    TestCase.assertEquals(27 + 6, tokens.size());
    TestCase.assertEquals(TokenType.OPEN_TAG, tokens.get(0).getType());
  }
}
