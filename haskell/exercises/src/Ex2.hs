module Ex2 where

sumOddR :: (Integral  a) => [a] -> a

sumOddR [x] = if odd x then x else 0
sumOddR (x:xs) = (if (odd x) then x else 0) + (sumOddR xs)

sumOddC :: (Integral a) => [a] -> a
sumOddC xs = foldl (+) 0 (filter (odd) xs)
