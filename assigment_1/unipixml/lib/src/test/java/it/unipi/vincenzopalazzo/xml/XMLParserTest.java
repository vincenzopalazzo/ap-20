package it.unipi.vincenzopalazzo.xml;

import it.unipi.vincenzopalazzo.xml.strategies.decoder.ast.INode;
import it.unipi.vincenzopalazzo.xml.strategies.decoder.ast.visitor.PrintVisitor;
import it.unipi.vincenzopalazzo.xml.strategies.decoder.parser.XMLParser;
import it.unipi.vincenzopalazzo.xml.strategies.decoder.scanner.Token;
import it.unipi.vincenzopalazzo.xml.strategies.decoder.scanner.XMLScanner;
import java.util.List;
import junit.framework.TestCase;
import org.junit.Test;

public class XMLParserTest {

  @Test
  public void simpleParsing() {
    String portionXml =
        "<Student>\n"
            + "\t<firstName type=\"String\">Vincenzo</firstName>\n"
            + "\t<age type=\"int\">24</age>\n"
            + "</Student>";
    PrintVisitor printer = new PrintVisitor();
    XMLScanner scanner = new XMLScanner(portionXml);
    List<Token> tokens = scanner.scanTokens();
    XMLParser parser = new XMLParser(tokens);
    INode ast = parser.parser();
    ast.accept(printer);
    TestCase.assertNotNull(ast);
  }

  @Test
  public void completeParsing() {
    String portionXml =
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
            + "<Students>\n"
            + "<Student>\n"
            + "\t<firstName type=\"String\">Vincenzo</firstName>\n"
            + "\t<age type=\"int\">24</age>\n"
            + "</Student>\n"
            + "</Students>";
    PrintVisitor printer = new PrintVisitor();
    XMLScanner scanner = new XMLScanner(portionXml);
    List<Token> tokens = scanner.scanTokens();
    XMLParser parser = new XMLParser(tokens);
    INode ast = parser.parser();
    ast.accept(printer);
    TestCase.assertNotNull(ast);
  }
}
