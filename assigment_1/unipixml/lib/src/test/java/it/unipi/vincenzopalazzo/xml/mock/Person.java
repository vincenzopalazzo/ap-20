package it.unipi.vincenzopalazzo.xml.mock;

import it.unipi.vincenzopalazzo.xml.annotations.XMLabel;
import it.unipi.vincenzopalazzo.xml.annotations.XMLfield;

@XMLabel
public class Person {
  @XMLfield(type = "String")
  protected String firstName;

  @XMLfield(type = "String", name = "surname")
  protected String lastName;

  @XMLfield(type = "int")
  protected int age;

  public Person() {}

  public Person(String firstName, String lastName, int age) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.age = age;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public int getAge() {
    return age;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Person person = (Person) o;

    if (age != person.age) return false;
    if (firstName != null ? !firstName.equals(person.firstName) : person.firstName != null)
      return false;
    return lastName != null ? lastName.equals(person.lastName) : person.lastName == null;
  }

  @Override
  public int hashCode() {
    int result = firstName != null ? firstName.hashCode() : 0;
    result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
    result = 31 * result + age;
    return result;
  }
}
