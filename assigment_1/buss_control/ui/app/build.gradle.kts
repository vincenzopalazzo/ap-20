plugins {
    application
    maven

    id("com.github.sherter.google-java-format") version "0.9"
}

repositories {
    jcenter()
}

dependencies {
    implementation(files("devlib/CovidController.jar"))
    implementation(files("devlib/bus.jar"))

    implementation("io.github.vincenzopalazzo:material-ui-swing:1.1.2-rc2")
    implementation("io.github.material-ui-swing:SwingSnackBar:0.0.2")
    implementation("io.github.material-ui-swing:DarkStackOverflowTheme:0.0.1-rc3")
    implementation("ch.qos.logback:logback-classic:1.2.3")
    implementation("ch.qos.logback:logback-core:1.2.3")
    implementation("org.slf4j:slf4j-api:1.7.25")

    testImplementation("junit:junit:4.13")
}

application {
    // Define the main class for the application.
    mainClass.set("it.unipi.vincenzopalazzo.buscovid.App")
}

tasks {
    register("fatJar", Jar::class.java) {
        archiveClassifier.set("all")
        duplicatesStrategy = DuplicatesStrategy.EXCLUDE
        manifest {
            attributes("Main-Class" to application.mainClass)
        }
        from(
            configurations.runtimeClasspath.get()
                .onEach { println("add from dependencies: ${it.name}") }
                .map { if (it.isDirectory) it else zipTree(it) }
        )
        val sourcesMain = sourceSets.main.get()
        sourcesMain.allSource.forEach { println("add from sources: ${it.name}") }
        from(sourcesMain.output)
    }
}

