package it.unipi.vincenzopalazzo.xml.mock;

import it.unipi.vincenzopalazzo.xml.annotations.XMLfield;

public class Student extends Person {

  @XMLfield(type = "String", name = "surname")
  private String clazz;

  public Student() {}

  public Student(String firstName, String lastName, int age, String clazz) {
    super(firstName, lastName, age);
    this.clazz = clazz;
  }

  @Override
  public String toString() {
    return String.format("%s %s %d class %s", this.firstName, this.lastName, this.age, this.clazz);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Student)) return false;
    if (!super.equals(o)) return false;

    Student student = (Student) o;

    return clazz != null ? clazz.equals(student.clazz) : student.clazz == null;
  }

  @Override
  public int hashCode() {
    int result = super.hashCode();
    result = 31 * result + (clazz != null ? clazz.hashCode() : 0);
    return result;
  }
}
