"""
Structure of the implementation inspired to the Google benchmark
library https://github.com/google/benchmark

This file contains a couple of example to run the benchmark to make
some measurements on some sort function.

In addition, are making some interesting benchmark on (c)python threading
library.

TODO add discussion.
"""
import random
import threading
from lib import benchmark
from sort import merge_sort


@benchmark(warmups=20, verbose=True)
def benchmark_one():
    """
    This benchmark do nothing
    """
    a = 1
    a += 1


@benchmark(verbose=True, iter=20)
def benchmark_sort_stl():
    """
    Benchmark on the sort algorithm of the (c)python standard
    library.
    """
    input_array = [random.randint(i, 900000) for i in range(90000)]
    input_array.sort()


@benchmark(verbose=True, iter=20)
def benchmark_craft_sort():
    """
    Benchmark on a merge sort python implementation
    taken from GitHub only to have a match with the
    sort algorithm of the (c)python standard library
    """
    input_array = [random.randint(i, 900000) for i in range(90000)]
    merge_sort(input_array)


def muiltithreadin_benchmark(func, parallel_degree, iter) -> object:
    """
    Te function permit to test an argument func, iterated "iter times" in a "parallel_degree threads".
    :param func: The "under test function", passed as arguments to the High-order function test.
    :param parallel_degree: Number of parallel thread
    :param iter: Number of the iteration of execution of the func
    :return: the thread_wrapper, function that wait for the end of all threads
    """

    @benchmark(
        warmups=0,
        iter=10,
        verbose=False,
        csv_file="./f_" + str(parallel_degree) + "_" + str(iter) + ".csv",
    )
    def thread_wrapper():
        """
        A function that wait for the end of all threads. The benchmark is set for only 1 iteration, no warmups,
        verbose false and a pattern for filename as "f_parallel_degree_iter.csv".
        :return:
        """

        def func_wrapper():
            """
            Function wrapper for handle the "under test func" iteration executions
            :return: None
            """
            for _ in range(iter):
                func()

        # list for handle all thread instances
        threads_list = []

        for index in range(parallel_degree):
            tmp_thread = threading.Thread(target=func_wrapper)
            threads_list.append(tmp_thread)
            tmp_thread.start()
        # Wait that all thread will finish
        for index, thread in enumerate(threads_list):
            thread.join()

    thread_wrapper()


def fib(n=5):
    """
    Function that implement the algorithm to calculate the Fibonacci number.
    :param n: the number to calc Fibonacci sequence
    :return: the Fibonacci number
    """
    if n == 0 or n == 1:
        return 1
    else:
        return fib(n - 1) + fib(n - 2)


@benchmark(warmups=1, iter=5, verbose=True, csv_file="./fibwrapper.csv")
def benchmark_fibonacci_number(n=5):
    """
    Benchmark of the trivial implementation of the fibonacci number
    algorithm
    :param n: the Fibonacci number
    :return: the Fibonacci number
    """
    fib(n)  # Ingore the result of the function, we care only the time


if __name__ == "__main__":
    """
    All the method inside this function need to be
    the benchmark with all the decorator @benchmark(...).

    In addition, the following benchmarks include also a measurement of performance
    with (c)python multithreading. But with GIL system there are lock on threading
    and the execution is running is managed by the interpreter in a sequential way.
    The following is the resul of multithreading running on a machine with 4 core

    Average Time is  0.5025265693664551  with  10 iteration
    Result stored in the file named ./f_1_16.csv

    Average Time is  0.5127949714660645  with  10 iteration
    Result stored in the file named ./f_2_8.csv

    Average Time is  0.4990102529525757  with  10 iteration
    Result stored in the file named ./f_4_4.csv

    Average Time is  0.4913187980651855  with  10 iteration
    Result stored in the file named ./f_8_2.csv


    From this source there are some implementation (java and C#) that contains
    the support of the real multithreading, so with this implementation could be
    an improvement
    https://stackoverflow.com/questions/44793371/is-multithreading-in-python-a-myth
    """
    benchmark_one()
    benchmark_sort_stl()
    benchmark_craft_sort()
    benchmark_fibonacci_number(20)
    fib_n = 25
    muiltithreadin_benchmark(lambda: fib(fib_n), 1, 16)
    muiltithreadin_benchmark(lambda: fib(fib_n), 2, 8)
    muiltithreadin_benchmark(lambda: fib(fib_n), 4, 4)
    muiltithreadin_benchmark(lambda: fib(fib_n), 8, 2)
