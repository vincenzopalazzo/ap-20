{-# LANGUAGE TemplateHaskell #-}
import           Ex1
import           Ex2

import           Data.List
import           Test.HUnit

testFoldl = TestCase $ assertEqual "foldl (+) 0 (fromList [1,2,1,2,3,4,3,4,3]) == 10" 10 (foldl (+) 0 (fromList [1,2,1,2,3,4,3,4,3]))
testFoldr = TestCase $ assertEqual "foldr (:) [] (fromList [1,2,1,2,3,4,3,4,3]) == [4, 3, 2, 1]" [4, 3, 2, 1] (foldr (:) [] (fromList [1,2,1,2,3,4,3,4,3]))
testLength = TestCase $ assertEqual "length (fromList [1,2,1,2,3,4,3,4,3]) == 4" 4 (length (fromList [1,2,1,2,3,4,3,4,3]))
testElemTrue = TestCase $ assertEqual "elem 'b' (fromList \"abcdaad\") == True" True (elem 'b' (fromList "abcdaad"))
testElemFalse = TestCase $ assertEqual "elem 'e' (fromList \"abcdaad\") == False" False (elem 'e' (fromList "abcdaad"))
testMaximum = TestCase $ assertEqual "maximum (fromList \"abcdaad\") == 'd'" 'd' (maximum (fromList "abcdaad"))
testMinimum = TestCase $ assertEqual "minimum (fromList \"abcdaad\") == 'a'" 'a' (minimum (fromList "abcdaad"))

testMapLB = TestCase $ assertEqual "mapLB (\\x -> mod x 2) (fromList [1,2,1,2,2,2,1,1,1,1,2,2,3,3,3,4,4]) == LB [(1,9),(0,8)]" (LB [(1,8),(0,9)]) (mapLB (\x -> mod x 2) (LB [(2, 9), (1, 8)]))

testlist = TestList [TestLabel "testFoldl" testFoldl,
                     TestLabel "testFoldr" testFoldr,
                     TestLabel "testLength" testLength,
                     TestLabel "testElemTrue" testElemTrue,
                     TestLabel "testElemFalse" testElemFalse,
                     TestLabel "testMaximum" testMaximum,
                     TestLabel "testMinimum" testMinimum,
                     TestLabel "testMapLB" testMapLB
                     ]


-- Main
main :: IO ()
main = do
  runTestTT testlist
  return ()
